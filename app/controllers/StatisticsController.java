package controllers;

import cn.bran.play.JapidController;
import models.*;
import models.enums.GoodStatus;
import org.apache.commons.lang.StringUtils;
import play.mvc.Before;
import utils.CommonUtils;
import vos.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by daikai on 2017/8/30.
 */
public class StatisticsController extends JapidController{

    @Before
    private static void checkPerson(){
        if(CommonUtils.currPerson() == null){
            Application.loginPage();
        }
    }


    /**
     * 巡查对象情况
     */
    public static void statis1(){
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);

        List<StaOneVO> staOneVOS = StaOneVO.convertFromOrgList(organizeGroups);
        renderJapid(staOneVOS);
    }


    /**
     * 巡查对象详情
     */
    public static void statis2(){
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);

        List<OrganizeGroupGoods> organizeGroupGoods = OrganizeGroupGoods.fetchAllForStatist(organizeGroups);
        renderJapid(organizeGroupGoods);
    }


    /**
     * 企业详情
     */
    public static void statis3(){
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
        List<OrganizeGroupVO> vos = OrganizeGroupVO.toListByStatist(organizeGroups);
        renderJapid(vos);
    }


    /**
     * 非正常内容统计
     */
    public static void statis4(){
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
        List<OrganizeGroupGoods> organizeGroupGoods = OrganizeGroupGoods.fetchByOrgsAndStatus(GoodStatus.非正常,organizeGroups);
        organizeGroupGoods.addAll(OrganizeGroupGoods.fetchByOrgsAndStatus(GoodStatus.维修中,organizeGroups));
        List<StaFourVO> staFourVOS  = StaFourVO.convertFromOggList(organizeGroupGoods);
        renderJapid(staFourVOS);
    }

    /**
     * 非正常统计
     */
    public static void statis5(){
        Person person = CommonUtils.currPerson();
        List<StaFiveVO> staFiveVOS  = new ArrayList<>();
        OrganizeGroupPerson ogp = OrganizeGroupPerson.findByPerson(person);
        if(ogp.group.isPlat){
            PersonRole pr = PersonRole.findByPerson(person);
            List<Region> regions = Region.getchildrens(pr.role.region);
            List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
            staFiveVOS = StaFiveVO.toVOListByGroups(organizeGroups);
        }else{
            staFiveVOS = StaFiveVO.toVOList(ogp.group);
        }

        renderJapid(staFiveVOS,ogp.group);
    }


    /**
     * 维修，报废数量统计
     */
    public static void statis6(){
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
        List<StaSixVO> staSixVOS = StaSixVO.convertFromOrgList(organizeGroups);
        renderJapid(staSixVOS);
    }

    /**
     * 维修对象统计
     */
    public static void statis7(){
        String today = CommonUtils.formatDate(new Date(),"yyyy-MM-dd");
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
        List<OrganizeGroupGoods> organizeGroupGoods = OrganizeGroupGoods.fetchByOrgsAndStatus(GoodStatus.维修中,organizeGroups);
        List<StaSeven> staSevens = StaSeven.toList(organizeGroupGoods);
        renderJapid(staSevens,today);
    }

    /**
     *企业人员统计
     */
    public static void statis8(){
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);

        List<OrganizeGroupVO> organizeGroupVOS = OrganizeGroupVO.toListByStatist(organizeGroups);
        renderJapid(organizeGroupVOS);
    }

    /**
     *企业详情统计
     */
    public static void statis9(){
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
        renderJapid(organizeGroups);
    }

    /**
     *供应商统计
     */
    public static void statis10(){
        List<Supplier> suppliers = Supplier.fetchAll();
        List<SupplierVO> supplierVOS = SupplierVO.convertFromSuppliers(suppliers);
        renderJapid(supplierVOS);
    }



    public static void statis11(){
        Person person = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
        List<StaElevenVO> staElevenVOS = StaElevenVO.toList(organizeGroups);
        renderJapid(staElevenVOS);
    }



    //json页面
    public static void jsonStatis5(long regionId){
        List<Region> regions = Region.getchildrens(Region.findById(regionId));
        List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
        List<StaFiveVO> staFiveVOS = StaFiveVO.toVOListByGroups(organizeGroups);
        renderJapid(staFiveVOS);
    }







    //*****************弹窗页面******************
    public static void floorPicPage(long orgId){
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        List<FloorPics> fps = OrganizeGroupPics.fetchByGroup(group);
        renderJapid(fps);
    }


    public static void xjPersPage(long orgId){
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        List<Person> personList = OrganizeGroupPerson.findPersonbyGroup(group);
        personList = personList.stream().filter(p -> (StringUtils.isNotBlank(p.job)&& ("巡检员").equals(p.job) ))
                .collect(Collectors.toList());
        renderJapid(personList);
    }

    public static void aqPersPage(long orgId){
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        List<Person> personList = OrganizeGroupPerson.findPersonbyGroup(group);
        personList = personList.stream().filter(p -> (StringUtils.isNotBlank(p.job)&& ("安全员").equals(p.job) ))
                .collect(Collectors.toList());
        renderJapidWith("@xjPersPage",personList);
    }

    public static void fzPersPage(long orgId){
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        List<Person> personList = OrganizeGroupPerson.findPersonbyGroup(group);
        personList = personList.stream().filter(p -> (StringUtils.isNotBlank(p.job)&& ("负责人").equals(p.job) ))
                .collect(Collectors.toList());
        renderJapidWith("@xjPersPage",personList);
    }


}
