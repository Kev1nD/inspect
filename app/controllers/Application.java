package controllers;

import cn.bran.play.JapidController;
import cn.bran.play.JapidPlayAdapter;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.*;
import models.enums.GoodStatus;
import models.enums.InspectStatus;
import models.enums.RecordStatus;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.FileUpload;
import play.data.parsing.DataParser;
import play.mvc.Before;
import utils.*;
import vos.*;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class Application extends JapidController{

    public static void index(Long groupId) {
        Person person =CommonUtils.currPerson();
        OrganizeGroup org = null;
        if(groupId != null){
            org = OrganizeGroup.findById(groupId);
        }else{
            OrganizeGroupPerson ogp = OrganizeGroupPerson.findByPerson(person);
            org = ogp.group;
        }
        if(!org.isPlat){
            orgIndex(org.id);
        }
        PersonRole personRole = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(personRole.role.region);
        List<OrganizeGroup> groups = OrganizeGroup.fetchByRegions(regions);
        renderJapid(org,groups,person);
    }


    public static void orgIndex(Long groupId){
        Date today = new Date();
        OrganizeGroup group = OrganizeGroup.findById(groupId);
        //到了检查时间没有检查的，即状态是已过期的
        List<GoodsLog> expireGoods = GoodsLog.findByGroupAndStatus(group,InspectStatus.未巡检);
        //非正常且没维修的
        List<GoodsLog> abnormalGoods = GoodsLog.findAbnormalByGroupAndStatus(group,InspectStatus.非正常);
        //到了维修时间没有维修的
        List<Goods> norepairGoods = OrganizeGroupGoods.findNoRepairGoodsByGroup(group);
        String nowTimeStr = CommonUtils.formatDate(today,"yyyy-MM-dd");
        //到了报废时间没有报废的
        List<Goods> noscrapGoods = OrganizeGroupGoods.findScrapGoodsByGroup(group,  nowTimeStr);
        List<AbnormalGoodsVO> vos = new ArrayList<>();
        vos.addAll(AbnormalGoodsVO.fromLog(expireGoods));
        vos.addAll(AbnormalGoodsVO.fromLog(abnormalGoods));
        vos.addAll(AbnormalGoodsVO.fromGoods(norepairGoods));
        vos.addAll(AbnormalGoodsVO.fromGoods(noscrapGoods));
        renderJapid(vos,group);
    }

    public static void loginPage(){
        renderJapid();
    }

    //巡检对象
    public static void goodsPage(long groupId,Long goodsId){
        OrganizeGroup org = OrganizeGroup.findById(groupId);
        List<Goods> goodsList = new ArrayList<>();
        List<OrganizeGroup> organizeGroups = new ArrayList<>();
        if(org.isPlat){
            Person person = CommonUtils.currPerson();
            PersonRole pr = PersonRole.findByPerson(person);
            if (pr.role.roleName.equals("平台管理员")){
               // goodsList = OrganizeGroupGoods.fetchAll().stream().map(ogg ->ogg.goods).collect(Collectors.toList());
                goodsList = OrganizeGroupGoods.findByCondition(org,null,goodsId);
            }else{
                List<Region> regions = Region.getchildrens(pr.role.region);
                organizeGroups = OrganizeGroup.fetchByRegions(regions);
                //goodsList = OrganizeGroupGoods.findByGroups(organizeGroups).stream().map(ogg ->ogg.goods).collect(Collectors.toList());
                goodsList = OrganizeGroupGoods.findByCondition(org,organizeGroups,goodsId);
            }

        }else{
            //goodsList = OrganizeGroupGoods.findByGroup(org);
            organizeGroups.add(org);
            goodsList = OrganizeGroupGoods.findByCondition(org,organizeGroups,goodsId);
        }
        //绑定巡检地点
        List<Area> areas = OrganizeGroupArea.findByGroup(org);
        //绑定生产单位
        List<Supplier> suppliers = OrganizeGroupSupplier.findByGroup(org);
        //检查对象名称
        List<GoodsName> goodsNames = GoodsName.fetchAll();
        List<Abnormal> abnormals = Abnormal.fetchAll();
        String today = CommonUtils.formatDate(new Date(),"yyyy-MM-dd");
        renderJapid(org,goodsList,areas,suppliers,today,goodsNames,abnormals);
    }

    //巡检区域
    public static void areaPage(long groupId,Long areaId,Long checkId){
        OrganizeGroup org = OrganizeGroup.findById(groupId);
        List<Area> areas = null;
        List<OrganizeGroup> organizeGroups = new ArrayList<>();
        if(org.isPlat){
            Person person = CommonUtils.currPerson();
            PersonRole pr = PersonRole.findByPerson(person);
            if (pr.role.roleName.equals("平台管理员")){
                //areas = OrganizeGroupArea.fetchAll();
                areas = OrganizeGroupArea.findByCondition(org,null,areaId,checkId);
            }else{
                List<Region> regions = Region.getchildrens(pr.role.region);
                organizeGroups = OrganizeGroup.fetchByRegions(regions);
                //areas = OrganizeGroupArea.findByGroups(organizeGroups).stream().map(ogg ->ogg.area).collect(Collectors.toList());
                areas = OrganizeGroupArea.findByCondition(org,organizeGroups,areaId,checkId);
            }

        }else{
            //areas = OrganizeGroupArea.findByGroup(org);
            organizeGroups.add(org);
            areas = OrganizeGroupArea.findByCondition(org,organizeGroups,areaId,checkId);
        }
        List<Person> persons = OrganizeGroupPerson.findPersonbyGroup(org);
        //筛选巡检员
        persons = persons.stream().filter(p -> !p.resources.equals("all"))
                         .filter(p -> p.job != null && p.job.equals("巡检员")).collect(Collectors.toList());

        //Map<Area,List<Goods>> agMap = Area.getAGMap();
//        Map<Area,String> map = new HashMap<>();
//        for(Map.Entry<Area,List<Goods>> entry:agMap.entrySet()){
//            List<String> typeStr = entry.getValue().stream().map(g -> g.name).distinct().collect(Collectors.toList());
//            String type = StringUtils.join(typeStr,",");
//            map.put(entry.getKey(),type);
//        }
        renderJapid(org,persons,areas);
    }

    //人员管理
    public static void personPage(long groupId){
        OrganizeGroup org = OrganizeGroup.findById(groupId);
        Person person = CommonUtils.currPerson();
        List<OrganizeGroupPerson> ogps = new ArrayList<>();
        if(org.isPlat){

            PersonRole pr = PersonRole.findByPerson(person);
            if (pr.role.roleName.equals("平台管理员")){
                ogps = OrganizeGroupPerson.fetchAll();
            }else{
                List<Region> regions = Region.getchildrens(pr.role.region);
                List<OrganizeGroup> organizeGroups = OrganizeGroup.fetchByRegions(regions);
                organizeGroups.remove(org);
                ogps = OrganizeGroupPerson.findPersonbyGroups(organizeGroups);
            }
        }else{
            ogps = OrganizeGroupPerson.findOrgPersonbyGroup(org);
        }
        //筛选管理员
        ogps = ogps.stream().filter(p -> !p.person.resources.equals("all")).collect(Collectors.toList());
        List<Person> persons  = ogps.stream().map(ogp -> ogp.person).collect(Collectors.toList());
        List<Person> fzPersons = persons.stream().filter(p -> StringUtils.isNotBlank(p.job) && p.job.equals("负责人")).collect(Collectors.toList());
        //获取角色
        List<Role> roles = Role.findNotAdminByGroup(org);
        renderJapid(org,ogps,fzPersons,roles,person);
    }

    public static void platPersonPage(){
        Person person = CommonUtils.currPerson();
        PersonRole personRole = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(personRole.role.region);
        OrganizeGroup org = OrganizeGroup.findPlat();
        List<PersonRole> personRoles = new ArrayList<>();
        personRoles.add(personRole);
        personRoles.addAll(PersonRole.fetchByRoleRegion(org,regions));
        //筛选管理员
        personRoles = personRoles.stream().filter(pr ->!pr.person.resources.equals("all")).collect(Collectors.toList());
        //获取角色
        List<Role> roles = Role.findByRegions(regions);
        renderJapid(org,personRoles,roles,person);
    }


    //角色管理
    public static void rolePage(long groupId){
        Person person = CommonUtils.currPerson();
        OrganizeGroup org = OrganizeGroup.findById(groupId);
        List<Resource> resourceList = Resource.fetchAll();
        List<Role> roles = new ArrayList<>();
        PersonRole pr = PersonRole.findByPerson(person);
        List<Region> regions = Region.getchildrens(pr.role.region);
        if(org.isPlat){
            roles = Role.findByRegions(regions);
        }else{
            resourceList = resourceList.stream().filter(r -> !r.code.contains("s") && !r.code.equals("o4")).collect(Collectors.toList());
            resourceList.add(Resource.findbycode("s5"));
            roles = Role.findByGroup(org);
        }
        renderJapid(org,resourceList,roles,person,pr);
    }

    //供应商管理
    public static void supplierPage(long groupId){
        OrganizeGroup org = OrganizeGroup.findById(groupId);
        List<OrganizeGroupSupplier> suppliers = OrganizeGroupSupplier.findAllByGroup(org);
        renderJapid(org,suppliers);
    }

    //二维码页面
    public static void barcodePage(){
       // List<QrCode> qrCodes = QrCode.fetchBysize(1,10);
        renderJapid();
    }

    //非正常选项配置
    public static void abnormalPage(){
        List<Abnormal> abnormals = Abnormal.fetchAll();
        renderJapid(abnormals);
    }

    //建筑结构类型配置
    public static void structurePage(){
        List<Structure> structures = Structure.fetchAll();
        renderJapid(structures);
    }

    //机构巡检对象型号配置
    public static void orgGoodsTypePage(long groupId){
        OrganizeGroup group = OrganizeGroup.findById(groupId);
        List<GoodsType> goodsTypes = GoodsType.fetchByGroup(group);
        renderJapid(group,goodsTypes);
    }


    //巡检对象名称配置
    public static void goodsNamePage(){
        List<GoodsName> goodsNames = GoodsName.fetchAll();
        renderJapid(goodsNames);
    }


    //新增企业
    public static void addOrgPage(long regionId){
        Region region = Region.findById(regionId);
        List<Structure> structures = Structure.fetchAll();
        renderJapid(structures,region);
    }

    //编辑企业
    public static void editOrgPage(long orgId){
        Person person = CommonUtils.currPerson();
        List<Structure> structures = Structure.fetchAll();
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        PersonRole personRole = PersonRole.findByPerson(person);
        renderJapid(group,structures,personRole);
    }


    //企业信息详情
    public static void orgInfoPage(long orgId){
        Person person = CommonUtils.currPerson();
        List<Structure> structures = Structure.fetchAll();
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        PersonRole personRole = PersonRole.findByPerson(person);
        renderJapid(group,structures,personRole);
    }

    //巡检记录查询
    public static void recordPage(long groupId,Long areaId,Long goodsId){
        OrganizeGroup org = OrganizeGroup.findById(groupId);
        List<GoodsLog> goodsLogs = null;
        List<OrganizeGroup> organizeGroups = new ArrayList<>();
        if(org.isPlat){
            Person person = CommonUtils.currPerson();
            PersonRole pr = PersonRole.findByPerson(person);
            if (pr.role.roleName.equals("平台管理员")){
                //goodsLogs = GoodsLog.fetchAll();
                goodsLogs = GoodsLog.findByCondition(org,null,goodsId,areaId);
            }else{
                List<Region> regions = Region.getchildrens(pr.role.region);
                organizeGroups = OrganizeGroup.fetchByRegions(regions);
                //goodsLogs = GoodsLog.findByGroups(organizeGroups);
                goodsLogs = GoodsLog.findByCondition(org,organizeGroups,goodsId,areaId);
            }

        }else{
           // goodsLogs = GoodsLog.findByGroup(org);
            organizeGroups.add(org);
            goodsLogs = GoodsLog.findByCondition(org,organizeGroups,goodsId,areaId);
        }
        renderJapid(org,goodsLogs);
    }

    //巡检对象维修记录弹框
    public static void repairLogPage(long goodsId){
        Goods goods = Goods.findById(goodsId);
        List<RepairLog> repairLogs = RepairLog.fetchByGoods(goods);
        renderJapid(goods,repairLogs);
    }

    //巡检对象维修弹框
    public static void repairPage(long goodsId){
        Goods goods = Goods.findById(goodsId);
        OrganizeGroup group = OrganizeGroupGoods.findByGoods(goods);
        List<Supplier> suppliers = OrganizeGroupSupplier.findByGroup(group);
        renderJapid(goods,suppliers);
    }

    //编辑平台用户页面
    public static void editPlatPerPage(Long personId){
        Person currPerson = CommonUtils.currPerson();
        PersonRole personRole = PersonRole.findByPerson(currPerson);
        List<Region> regions = Region.getchildrens(personRole.role.region);
        regions.add(personRole.role.region);
        OrganizeGroup org = OrganizeGroup.findPlat();
        List<Role> roles = Role.findByRegions(regions);
        Person person = Person.findById(personId);
        renderJapid(person,roles);
    }

    //编辑用户页面
    public static void editPersonPage(long personId,long orgId){
        OrganizeGroup org = OrganizeGroup.findById(orgId);
        Person person = Person.findById(personId);
        List<Person> persons = OrganizeGroupPerson.findPersonbyGroup(org);
        //筛选管理员
        persons = persons.stream().filter(p -> !p.resources.equals("all")).collect(Collectors.toList());
        List<Person> fzPersons = persons.stream().filter(p -> p.job.equals("负责人") && p != person).collect(Collectors.toList());
        List<Role> roles = Role.findByGroup(org);

        PersonRole pr = PersonRole.findByPerson(person);
        renderJapid(person,roles,fzPersons,pr);
    }

    //编辑巡检地点弹出框
    public static void editAreaPage(long areaId,long orgId){
        Area area = Area.findById(areaId);
        OrganizeGroup org =OrganizeGroup.findById(orgId);
        List<Person> persons = OrganizeGroupPerson.findPersonbyGroup(org);
        //筛选巡检员
        persons = persons.stream().filter(p -> p.job != null && p.job.equals("巡检员")).collect(Collectors.toList());
        renderJapid(area,persons);
    }


    //编辑巡检对象弹出框
    public static void editGoodsPage(long goodsId,long orgId){
        Goods goods = Goods.findById(goodsId);
        OrganizeGroup org =OrganizeGroup.findById(orgId);
        //绑定巡检地点
        List<Area> areas = OrganizeGroupArea.findByGroup(org);
        //绑定生产单位
        List<Supplier> suppliers = OrganizeGroupSupplier.findByGroup(org);
        //检查对象型号
        List<GoodsName> goodsNames = GoodsName.fetchAll();
        List<Abnormal> abnormals = Abnormal.fetchAll();
        renderJapid(goods,areas,suppliers,goodsNames,abnormals);
    }


    public static void  addPlatGoodsPage(long orgId){
        OrganizeGroup org = OrganizeGroup.findById(orgId);
        //绑定巡检地点
        List<Area> areas = OrganizeGroupArea.findByGroup(org);
        //绑定生产单位
        List<Supplier> suppliers = OrganizeGroupSupplier.findByGroup(org);
        //检查对象型号
        List<GoodsType> goodsTypes = GoodsType.fetchByGroup(org);
        List<Abnormal> abnormals = Abnormal.fetchAll();
        renderJapid(org,areas,suppliers,abnormals,goodsTypes);
    }


    public static void  addPlatAreaPage(long orgId){
        OrganizeGroup org = OrganizeGroup.findById(orgId);
        List<Person> persons = OrganizeGroupPerson.findPersonbyGroup(org);
        //筛选巡检员
        persons = persons.stream().filter(p ->p.mobile.length() >4)
                .filter(p -> p.job != null && p.job.equals("巡检员")).collect(Collectors.toList());
        renderJapid(org,persons);
    }



    public static void addOrgPersonPage(long orgId){
        OrganizeGroup org  = OrganizeGroup.findById(orgId);
        List<Person> persons = OrganizeGroupPerson.findPersonbyGroup(org);
        List<Person> fzPersons = persons.stream().filter(p -> StringUtils.isNotBlank(p.job) && p.job.equals("负责人")).collect(Collectors.toList());
        //获取角色
        List<Role> roles = Role.findByGroup(org);
        renderJapid(org,fzPersons,roles);
    }



    public static void orgRolePage(){
        Person curperson = CommonUtils.currPerson();
        PersonRole pr = PersonRole.findByPerson(curperson);
        List<Region> regions = Region.getchildrens(pr.role.region);
        List<OrganizeGroup> groups = OrganizeGroup.fetchByRegions(regions);
        List<Role> roles = Role.findOrgRolesByGroups(groups);
        renderJapid(curperson,roles,pr);
    }


    public static void jsonAddRolePage(long orgId){
        List<Resource> resourceList = Resource.fetchAll();
        renderJapid(resourceList);
    }

    //新闻广告
    public static void newsNoticePage(){
        List<News> newsList = News.fetchAll();
        renderJapid(newsList);
    }


    public static void addNewsNoticePage(){
        renderJapid();
    }



    public static void editNewsNoticePage(long newsId){
        News news = News.findById(newsId);
        renderJapid(news);
    }


    //通知
    public static  void noticePage(long groupId){
        OrganizeGroup group = OrganizeGroup.findById(groupId);
        List<Notice> notices = new ArrayList<>();
        if(group.isPlat){
            Person person = CommonUtils.currPerson();
            PersonRole personRole = PersonRole.findByPerson(person);
            notices = Notice.fetchByRegion(personRole.role.region,1,Integer.MAX_VALUE);
        }else {
            notices = Notice.fetchByGroup(group, 1, Integer.MAX_VALUE);
        }
        renderJapid(group,notices);
    }

    public static void homePage(){
        Person person = CommonUtils.currPerson();
        OrganizeGroup plat = OrganizeGroup.findPlat();
        //广告轮播
        List<News> notices = News.fetchByType("1",1,3);
        //新闻
        List<News> news = News.fetchByType("0",1,10);
        //知识
        List<News> zslist = News.fetchByType("2",1,10);
        //上级通知公告
        List<Notice> upNotices = new ArrayList<>();
        //本级通知公告
        List<Notice> curNotices = new ArrayList<>();

        OrganizeGroupPerson ogp = OrganizeGroupPerson.findByPerson(person);

        //平台账号
        if(ogp.group.isPlat){
            PersonRole personRole = PersonRole.findByPerson(person);
            List<Region> regions =Region.getParents(personRole.role.region);
            upNotices = Notice.fetchByRegions(regions,1,10);
            curNotices =  Notice.fetchByRegion(personRole.role.region,1,10);
        }else{
            upNotices = Notice.fetchByGroup(plat,1,10);
            curNotices =  Notice.fetchByGroup(ogp.group,1,10);
        }
        renderJapid(news,notices,upNotices,curNotices,person,zslist);
    }



    public static void moreNewsPage(){
        List<News> news = News.fetchByType("1",1,Integer.MAX_VALUE);
        renderJapid(news);
    }

    public static void moreZsPage(){
        List<News> zslist = News.fetchByType("2",1,Integer.MAX_VALUE);
        renderJapid(zslist);
    }


    public static void newsDetailPage(long newsId){
        News news = News.findById(newsId);
        renderJapid(news);
    }


    public static void moreNoticesPage(){
        List<Notice> notices = Notice.fetchByGroup(OrganizeGroup.findPlat(),1,Integer.MAX_VALUE);
        renderJapid(notices);
    }

    public static void moreCurNoticesPage(){
        Person person = CommonUtils.currPerson();
        OrganizeGroupPerson ogp = OrganizeGroupPerson.findByPerson(person);
        List<Notice> notices = Notice.fetchByGroup(ogp.group,1,Integer.MAX_VALUE);
        renderJapidWith("@moreNoticesPage",notices);
    }


    public static void noticeDetailPage(long noticeId){
        Notice notice = Notice.findById(noticeId);
        Person person = CommonUtils.currPerson();
        NoticeLog.add(notice,person);
        renderJapid(notice);
    }


    public static void editSupplierPage(long supplierId){
        Supplier supplier = Supplier.findById(supplierId);
        renderJapid(supplier);
    }


    public static void viewPicPage(String url){
        renderJapid(url);
    }


    public static void personDetail(long personId){
        Person person = Person.findById(personId);
        PersonVO personVO = new PersonVO(person);
        renderJapid(personVO);
    }

    public static void supDetail(long supId){
        Supplier supplier = Supplier.findById(supId);
        renderJapid(supplier);
    }


    //巡检区域点击查看对象数量详情
    public static void goodsNumDetail(Long areaId){
        Area area = Area.findById(areaId);
        List<Goods> goodsList = Goods.findByArea(area);
        Map<String,Long[]> map = new HashMap<>();
        goodsList.stream().forEach(g ->{
            Long num1 = Goods.countGoodsByAreaAndGoodsName(area,g.name);
            Long num2 = GoodsLog.countGoodsNumByAreaAndName(area,g.name);
            Long[] numArray = {num1,num1-num2};
            map.put(g.name,numArray);
        });
        renderJapid(map);
    }


    //*******************页面部分结束*************************

    @Before(unless = {"loginPage","login","logout","uploadFile","getProvinces","getCityAndDistrict","getStreet","getOrgsByRegion"})
    private static void checkPerson(){
        if(CommonUtils.currPerson() == null){
            loginPage();
        }
    }


    public static void login(String userName,String password){
        Person person = Person.findByUserName(userName);
        if(person != null){
            if(StringUtils.equals(DigestUtils.md5Hex(password),person.password)){
                CommonUtils.savePersonToSession(person.id);
                OrganizeGroupPerson orp = OrganizeGroupPerson.findByPerson(person);
                if(orp == null){
                    renderJSON(ResultVO.failed("用户不存在！"));
                }
                String url = JapidPlayAdapter.lookupAbs("Application.homePage");
                renderJSON(ResultVO.succeed(url));
            }else{
                renderJSON(ResultVO.failed("密码错误！"));
            }
        }else{
            renderJSON(ResultVO.failed("用户不存在！"));
        }
    }

    public static void logout(){
        CommonUtils.savePersonToSession(null);
        String url = JapidPlayAdapter.lookupAbs("Application.homePage");
        renderJSON(ResultVO.succeed(url));
    }



    //新增企业
    public static void addOrg(OrganizeGroupVO orgVo){
        if (OrganizeGroup.findByName(orgVo.name) != null){
            renderJSON(ResultVO.failed("企业已经存在！"));
        }
        if(Person.findByUserName(orgVo.orgAdminName) != null){
            renderJSON(ResultVO.failed("企业管理员账号已经被注册！"));
        }
        OrganizeGroup group = OrganizeGroup.add(orgVo);
        if (StringUtils.isNotBlank(orgVo.picIds)){
            String[] picStrs = orgVo.picIds.split(",");
            for (String picStr:picStrs){
                FloorPics pic = FloorPics.findById(Long.valueOf(picStr));
                OrganizeGroupPics.add(group,pic);
            }
        }
        //自动注册企业管理员账号
        Person person = Person.regist(group.name+"管理员", orgVo.orgAdminName);
        OrganizeGroupPerson.add(group,person);
        //获取企业管理员角色
        Role orgAdminRole = Role.add(group,"企业管理员",null);
        PersonRole.add(orgAdminRole,person);
        person.editResource("all");
        renderJSON(ResultVO.succeed());
    }

    //编辑企业
    public static void editOrg(OrganizeGroupVO orgVo){
        OrganizeGroup group = OrganizeGroup.findById(orgVo.id);
        if (StringUtils.isNotBlank(orgVo.picIds)){
            String[] picStrs = orgVo.picIds.split(",");
            for (String picStr:picStrs){
                FloorPics pic = FloorPics.findById(Long.valueOf(picStr));
                OrganizeGroupPics ogp = OrganizeGroupPics.findByGroupAndPic(group,pic);
                if (ogp == null){
                    OrganizeGroupPics.add(group,pic);
                }
            }
        }
        group.edit(orgVo);
        renderJSON(ResultVO.succeed());

    }


    private static JsonArray convertRegionJson(List<Region> regions){
        JsonArray array = new JsonArray();
        regions.stream().forEach(p -> {
            JsonObject object = new JsonObject();
            object.addProperty("id",p.id);
            object.addProperty("name",p.name);
            array.add(object);
        });
        return array;
    }

    //获取省
    public static void getProvinces(){
        List<Region> provinces = Region.fetchByLevel("province");
        renderJSON(convertRegionJson(provinces));
    }

    //获取市,区
    public static void getCityAndDistrict(long parentId){
        Region region = Region.findById(parentId);
        List<Region> citys = Region.fetchByParent(region);
        renderJSON(convertRegionJson(citys));
    }

    //获取街道
    public static void getStreet(Long parentId){
        Region region = Region.findById(parentId);
        List<Region> streets = Region.fetchByParent(region);
        if(streets.isEmpty()){
            for(AmapUtils.AreaResult areaResult:AmapUtils.getStreet(region.adcode).districts){
                for (AmapUtils.AreaResult careaResult:areaResult.districts){
                    Region.add("", careaResult.adcode, careaResult.name, careaResult.center, careaResult.level, region);
                }
            }
            streets = Region.fetchByParent(region);
        }
        renderJSON(convertRegionJson(streets));

    }

    //新增用户
    public static void addPerson(Long orgId, PersonVO personVO,Long roleId){
        OrganizeGroup organizeGroup = OrganizeGroup.findById(orgId);
        Person person = Person.add(personVO);
        if (person == null){
            renderJSON(ResultVO.failed());
        }
        OrganizeGroupPerson.add(organizeGroup,person);
        Role role = Role.findById(roleId);
        PersonRole.add(role,person);
        List<Resource> resources = RoleResource.findByRole(role);
        String resourcesStr = role.roleName.equals("企业管理员")?"all":StringUtils.join(resources.stream().map(r ->r.code).collect(Collectors.toList()),",");
        person.editResource(resourcesStr);
        renderJSON(ResultVO.succeed());
    }

    //编辑用户
    public static void editPerson(PersonVO personVO,Long roleId){
        Person person = Person.findById(personVO.id);
        person.edit(personVO);
        PersonRole pr = PersonRole.findByPerson(person);
        Role role = Role.findById(roleId);
        if(pr == null){
            pr = PersonRole.add(role,person);
        }
        pr.editRole(role);
        List<Resource> resources = RoleResource.findByRole(role);
        String resourcesStr = role.roleName.equals("平台管理员")?"all":StringUtils.join(resources.stream().map(r ->r.code).collect(Collectors.toList()),",");
        person.editResource(resourcesStr);

        renderJSON(ResultVO.succeed());
    }

    //新增用户
    public static void addPlatPerson(PersonVO personVO,Long roleId){
        OrganizeGroup organizeGroup = OrganizeGroup.findPlat();
        Person person = Person.add(personVO);
        if (person == null){
            renderJSON(ResultVO.failed());
        }
        OrganizeGroupPerson.add(organizeGroup,person);
        Role role = Role.findById(roleId);
        PersonRole.add(role,person);
        List<Resource> resources = RoleResource.findByRole(role);
        String resourcesStr = role.roleName.equals("平台管理员")?"all":StringUtils.join(resources.stream().map(r ->r.code).collect(Collectors.toList()),",");
        person.editResource(resourcesStr);
        renderJSON(ResultVO.succeed());
    }

    //编辑用户
    public static void editPlatPerson(Long personId,String name,String mobile,Long roleId,String username){
        Person person = Person.findById(personId);
        person.editPlatPer(name,mobile,username);
        PersonRole pr = PersonRole.findByPerson(person);
        Role role = Role.findById(roleId);
        pr.editRole(role);
        List<Resource> resources = RoleResource.findByRole(role);
        String resourcesStr = role.roleName.equals("平台管理员")?"all":StringUtils.join(resources.stream().map(r ->r.code).collect(Collectors.toList()),",");
        person.editResource(resourcesStr);
        renderJSON(ResultVO.succeed());
    }


    //删除用户
    public static  void delPerson(long personId){
        Person person = Person.findById(personId);
        person.del();
        renderJSON(ResultVO.succeed());
    }


    //离职
    public static void personLeave(long personId){
        Person person = Person.findById(personId);
        person.setLeave();
        renderJSON(ResultVO.succeed());
    }



    //新增供应商
    public static void addSupplier(Long orgId, SupplierVO supplierVO){
        OrganizeGroup organizeGroup = OrganizeGroup.findById(orgId);
        Supplier supplier = Supplier.add(supplierVO);
        OrganizeGroupSupplier.add(organizeGroup,supplier);
        renderJSON(ResultVO.succeed());
    }


    //新增巡检地点
    public static void addArea(Long orgId, AreaVO areaVO){
        OrganizeGroup organizeGroup = OrganizeGroup.findById(orgId);
        Area area = Area.add(areaVO);
        if(area == null){
            renderJSON(ResultVO.failed());
        }
        OrganizeGroupArea.add(organizeGroup,area);
        renderJSON(ResultVO.succeed());
    }

    //编辑巡检地点
    public static void editArea(AreaVO areaVO){
        Area area = Area.findById(areaVO.id);
        area.edit(areaVO);
        renderJSON(ResultVO.succeed());
    }

    //新增巡检对象
    public static void addGoods(Long orgId,GoodsVO goodsVO){
        OrganizeGroup organizeGroup = OrganizeGroup.findById(orgId);
        Goods goods = Goods.add(goodsVO);
        OrganizeGroupGoods.add(organizeGroup,goods);
        renderJSON(ResultVO.succeed());
    }


    //编辑巡检对象
    public static  void editGoods(GoodsVO goodsVO){
        Goods goods = Goods.findById(goodsVO.id);
        goods.eidt(goodsVO);
        renderJSON(ResultVO.succeed());
    }

    //删除巡检对象
    public static void delGoods(long goodsId){
        Goods goods = Goods.findById(goodsId);
        goods.del();
        renderJSON(ResultVO.succeed());
    }

    //维修巡检对象
    public static void repairGoods(long goodsId,long supplierId,String content){
        Goods goods = Goods.findById(goodsId);
        Supplier supplier = Supplier.findById(supplierId);
        RepairLog.add(goods,supplier,content);
        goods.repair();
        renderJSON(ResultVO.succeed());
    }



    //编辑供应商
    public static  void editSupplier(SupplierVO supplierVO){
        Supplier supplier = Supplier.findById(supplierVO.id);
        supplier.edit(supplierVO);
        renderJSON(ResultVO.succeed());
    }

    //删除供应商
    public static  void delSupplier(long supplierId){
        Supplier supplier = Supplier.findById(supplierId);
        supplier.logicDelete();
        renderJSON(ResultVO.succeed());
    }


    //新增角色
    public static void addRole(long orgId, String roleName, Long regionId, Long[] resourceIds){
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        Region region = null;
        if (regionId != null){
             region = Region.findById(regionId);
        }
        Role role =  Role.add(group,roleName,region);
        for(Long resourceId:resourceIds){
            Resource resource = Resource.findById(resourceId);
            RoleResource.add(role,resource);
        }
        renderJSON(ResultVO.succeed());
    }

    //新增楼层弹窗
    public static void addFloorPicsPage(){
        renderJapid();
    }

    //增加楼层分布图
    public static void addFloorPics(String name,String pic){
        FloorPics fp = FloorPics.add(name,pic);
        renderJSON(ResultVO.succeed(fp.id));
    }


    public static void delFloorPic(long fpId){
        FloorPics floorPics = FloorPics.findById(fpId);
        floorPics.logicDelete();
        renderJSON(ResultVO.succeed());
    }


    public static void uploadFile() throws Exception{
        File file = null;
        DataParser parser = DataParser.parsers.get(request.contentType);
        if (parser != null) {
            // normal upload
            FileUpload qqfile = null;
            parser.parse(request.body);
            ArrayList<FileUpload> uploads = (ArrayList<FileUpload>) request.args.get("__UPLOADS");
            for (FileUpload upload : uploads) {
                if ("file".equals(upload.getFieldName())) {
                    qqfile = upload;
                    break;
                }
            }
            file = qqfile.asFile();
        } else {
            // XHR upload
            try {
                file = FileUploadUtils.getFileFromInput(request.params.get("file"));
            } catch (IOException e) {
                Logger.error(e, e.getMessage());
                file = null;
            }

        }
        Long curTime = System.currentTimeMillis();
        File imgfile = Play.getFile("/documentation/"+curTime+file.getName());
        File videofile = Play.getFile("/documentation/video/"+curTime+file.getName());
        String baseurl = Play.configuration.getProperty("application.baseUrl");

        //视频单独处理
        if(StringUtils.endsWith(file.getName(),"mp4")){
            file.renameTo(videofile);
            renderJSON(ResultVO.succeed(baseurl+"/file/video/"+videofile.getName()));
        }else{
            file.renameTo(imgfile);
            //图片按比例压缩
            PictureUtils.reduceImg(imgfile,null,null,0.5f);
            renderJSON(ResultVO.succeed(baseurl+"/file/"+imgfile.getName()));

        }

    }





    public static void downloadExcel(int num) throws  Exception{
        String[][] data = new String[num][2];
        String sql = " insert into QrCode (createTime,lastModifyTime,version,isDeleted,code,url) value(UNIX_TIMESTAMP()*1000,UNIX_TIMESTAMP()*1000,1,0,?,?)";
        Set<List<String>> params = new LinkedHashSet<>();
        String baseUrl = Play.configuration.getProperty("application.baseUrl");
        for(int i =0;i< num;i++){
            String code = RandomStringUtils.randomNumeric(10)+System.currentTimeMillis();
            String url = baseUrl+"/app/scan/"+code;
            params.add(Arrays.asList(code,url));
            data[i][0] = i+"";
            data[i][1] = url;
        }
        if(params != null && params.size() > 0){
            BatchSqlUtils.batchInsert(sql,new ArrayList<List<String>>(params));
        }
//        for(int i =0;i< num;i++){
//            QrCode code = QrCode.add();
//            data[i][0] = i+"";
//            data[i][1] = code.url;
//        }
        File tmpFile = Play.getFile("/template/barcodeTmp.xls");
        File file = Play.getFile("/tmp/data"+System.currentTimeMillis()+".xls");
        if(!file.exists()){
            FileUtils.copyFile(tmpFile,file);
        }
        ExcelUtil.export(data,file);
        renderBinary(file);

    }


    public static void delBarcode(long qcId){
        QrCode qrCode = QrCode.findById(qcId);
        qrCode.logicDelete();
        renderJSON(Result.succeed());
    }

    //巡检记录审核
    public static void reviewRecord(long goodsLogId,boolean isPass){
        GoodsLog log = GoodsLog.findById(goodsLogId);
        if (log.rstatus == null){
            renderJSON(ResultVO.failed());
        }
        if(log.rstatus == RecordStatus.待审核 || log.rstatus == RecordStatus.二次巡检 || log.rstatus == RecordStatus.过期巡检){
            log.setRstatus(isPass?RecordStatus.通过:RecordStatus.未通过);
            if(!isPass){
                log.editIStatus(InspectStatus.待巡检);
            }
            renderJSON(ResultVO.succeed());
        }else{
            renderJSON(ResultVO.failed());
        }
    }

    //修改密码
    public static void editPasswd(String oldPwd,String newPwd){
        Person person = CommonUtils.currPerson();
        if(!person.password.equals(DigestUtils.md5Hex(oldPwd))){
            renderJSON(ResultVO.failed());
        }
        person.editPasswd(newPwd);
        renderJSON(ResultVO.succeed());
    }

    //根据街道获取公司
    public static void getOrgsByRegion(long regionId){
       List<OrganizeGroup> groups= OrganizeGroup.fetchByRegions(Region.getchildrens(Region.findById(regionId)));
        JsonArray array = new JsonArray();
        groups.stream().forEach(p -> {
            JsonObject object = new JsonObject();
            object.addProperty("id",p.id);
            object.addProperty("name",p.name);
            array.add(object);
        });
        renderJSON(array);
    }


    //新增非正常项
    public static void addAbNormal(String name){
        Abnormal.add(name);
        renderJSON(ResultVO.succeed());
    }

    public static void delAbNormal(long abId){
        Abnormal abnormal  = Abnormal.findById(abId);
        abnormal.logicDelete();
        renderJSON(Result.succeed());
    }



    //新增建筑结构类型
    public static void addStructure(String name){
        Structure.add(name);
        renderJSON(ResultVO.succeed());
    }


    public static void delStructure(long structureId){
        Structure structure = Structure.findById(structureId);
        structure.logicDelete();
        renderJSON(ResultVO.succeed());
    }


    public static void addGoodsType(long orgId,String name){
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        GoodsType.add(name,group);
        renderJSON(ResultVO.succeed());
    }


    public static void delGoodsType(long gtId){
        GoodsType goodsType = GoodsType.findById(gtId);
        goodsType.logicDelete();
        renderJSON(ResultVO.succeed());

    }


    public static void delRole(long roleId){
        Role role = Role.findById(roleId);
        if(StringUtils.equals(role.roleName,"企业管理员")
                || StringUtils.equals(role.roleName,"平台管理员")){
            renderJSON(ResultVO.failed());
        }
        role.logicDelete();
        renderJSON(ResultVO.succeed());
    }


    public static void delArea(long areaId){
        Area area = Area.findById(areaId);
        area.logicDelete();
        renderJSON(ResultVO.succeed());
    }


    public static void delGroup(long orgId){
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        group.logicDelete();
        renderJSON(ResultVO.succeed());
    }


    //直接进入企业管理员账号
    public static void loginOrg(long orgId){
        Person oldPer = CommonUtils.currPerson();
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        Role role = Role.findBygroupAndName(group,"企业管理员");
        Person person = PersonRole.findByRole(role).person;
        CommonUtils.savePersonToSession(person.id);
        CommonUtils.savePlatSession(oldPer.id);
        renderJSON(ResultVO.succeed());
    }


    public static void backPlat(){
        Person person =Person.findById(CommonUtils.getFromPlatPersonId());
        CommonUtils.savePersonToSession(person.id);
        CommonUtils.savePlatSession(null);
        index(1l);
    }

    //新增新闻广告
    public static void addNewsNotice(String title,String content,String imgUrl,String type){
        News.add(title,content,imgUrl,type);
        renderJSON(ResultVO.succeed());
    }

    //删除新闻
    public static void delNews(long newsId){
        News news = News.findById(newsId);
        news.logicDelete();
        renderJSON(ResultVO.succeed());
    }


    //新增通知
    public static void addNotice(String title,String content,long groupId){
        OrganizeGroup group = OrganizeGroup.findById(groupId);
        if(group.isPlat){
            Person person = CommonUtils.currPerson();
            PersonRole personRole = PersonRole.findByPerson(person);
            Notice.add2(group,personRole.role.region,title,content);
        }else{
            Notice.add(group,title,content);
        }
        renderJSON(ResultVO.succeed());
    }


    public static void delNotice(long noticeId){
        Notice notice = Notice.findById(noticeId);
        notice.logicDelete();
        renderJSON(ResultVO.succeed());
    }

    public static void editNotice(long noticeId,String title,String content){
        Notice notice = Notice.findById(noticeId);
        notice.edit(title, content);
        renderJSON(ResultVO.succeed());
    }

    public static void getNotice(long noticeId){
        Notice notice = Notice.findById(noticeId);
        JsonObject object = new JsonObject();
        object.addProperty("title",notice.title);
        object.addProperty("content",notice.content);
        renderJSON(object);
    }


    public static void editNews(long newsId,String title,String content,String imgUrl,String type){
        News news = News.findById(newsId);
        news.edit(title,content,imgUrl,type);
        renderJSON(ResultVO.succeed());
    }



    public static void delRepairLog(long logId){
        RepairLog log  = RepairLog.findById(logId);
        log.logicDelete();
        renderJSON(ResultVO.succeed());
    }



    public static void addGoodsName(String name){
        GoodsName.add(name);
        renderJSON(ResultVO.succeed());
    }



    public static void delGoodsName(long goodsnameId){
        GoodsName goodsName = GoodsName.findById(goodsnameId);
        goodsName.logicDelete();
        renderJSON(ResultVO.succeed());
    }





}