package controllers;

import cn.bran.play.JapidController;
import models.*;
import play.Logger;
import play.Play;
import utils.PictureUtils;

import java.io.File;

/**
 * Created by daikai on 2017/7/8.
 */
public class DataController extends JapidController{

    public static void init(){
        Logger.info("数据初始化开始");
        Region.init();
        Resource.init();
        initGroup();
        Logger.info("数据初始化结束");

    }

    public static void initGroup(){
        OrganizeGroup plat = OrganizeGroup.findPlat();
        if (plat == null){
            plat = OrganizeGroup.addPlat();
            Person admin = Person.regist("全国管理员","0001");
            OrganizeGroupPerson.add(plat,admin);
            Role adminRole = Role.add(plat,"平台管理员",Region.findById(1l));
            admin.editResource("all");
            PersonRole.add(adminRole,admin);
        }

    }



    public static void flushPic(){
        File file = Play.getFile("/documentation");
        if(file.isDirectory() && file.listFiles() != null){
            System.err.println(file.listFiles().length);
            for (File f :file.listFiles()){
                PictureUtils.reduceImg(f,null,null,0.5f);
            }
        }
    }





}
