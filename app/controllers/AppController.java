package controllers;

import cn.bran.play.JapidController;
import models.*;
import models.enums.InspectStatus;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.data.FileUpload;
import play.data.binding.As;
import play.data.parsing.DataParser;
import utils.CommonUtils;
import utils.FileUploadUtils;
import vos.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * app接口
 * Created by daikai on 2017/7/1.
 */



public class AppController extends JapidController{


    public static void checkLogin(){
        Person person = CommonUtils.currPerson();
        if(person == null){
            renderJSON(Result.failed(Result.StatusCode.NOT_LOGIN_ERROR));
        }
        renderJSON(Result.succeed());
    }


    //app登陆
    public static void login(String mobile,String password){
        Person person = Person.findByUserName(mobile);
        if(person == null){
            renderJSON(Result.failed(Result.StatusCode.LOGIN_USER_NOTEXIST));
        }else{
            password = DigestUtils.md5Hex(password);
            if (!StringUtils.equals(password,person.password)){
               renderJSON(Result.failed(Result.StatusCode.LOGIN_PASSWORD_ERROR));
            }else{
                CommonUtils.savePersonToSession(person.id);
                response.setCookie("KEEP_LOGIN_ID", person.id + "", "365d");
                renderJSON(Result.succeed());
            }
        }

    }

    //app登出
    public static void logout(){
        CommonUtils.savePersonToSession(null);
        response.removeCookie("KEEP_LOGIN_ID");
        renderJSON(Result.succeed());
    }


    //获取未巡检的列表
    public static void uncheckGoods(){
        Person person = CommonUtils.currPerson();
        List<GoodsLog> goodsLogs = GoodsLog.findByChecker(person);
        List<GoodsVO> goodsVOS = GoodsVO.convertFromLog(goodsLogs);
        renderJSON(goodsVOS);
    }

    //巡检地点确认上传
    @Deprecated
    public static void setGoodsNum(Long areaId,int num){
        Area area = Area.findById(areaId);
        area.setGoodsNum(num);
        renderJSON(Result.succeed());
    }

    //巡检记录确认上传
    public static  void setGoodsLog(Long goodsId,String imgUrls,String videoUrl,String status,String reason){
        Goods goods = Goods.findById(goodsId);
        List<GoodsLog> logs = GoodsLog.findLogsByGoods(goods);
        for(GoodsLog log:logs){
            log.editGoodsLog(imgUrls,videoUrl,InspectStatus.valueOf(status),reason);
        }
        Person person = CommonUtils.currPerson();
        //该巡检点下所有记录
        List<Goods> checkgoods = Goods.findByArea(goods.area);
        List<GoodsLog> goodsLogs = GoodsLog.findByCheckerAndArea(person,checkgoods, InspectStatus.待巡检);
        if(goodsLogs.isEmpty()){
            //所有都巡检完
            renderJSON(Result.succeed("true"));
        }else{
            renderJSON(Result.succeed("false"));
        }
    }



    //绑定二维码---获取企业巡检点
    public static void getAreByOrg(){
        Person person = CommonUtils.currPerson();
        OrganizeGroupPerson ogp = OrganizeGroupPerson.findByPerson(person);
        List<Area> areas = OrganizeGroupArea.findByGroup(ogp.group);
        List<AreaVO> areaVOS = AreaVO.convertTolist(areas);
        renderJSON(areaVOS);
    }

    //绑定二维码--根据企业巡检点获取巡检对象
    public static void getGoodsByArea(Long areaId){
        Area area = Area.findById(areaId);
        List<Goods> goods = Goods.findByArea(area);
        List<GoodsVO> goodsVOS = GoodsVO.convertFromGoods(goods);
        renderJSON(goodsVOS);
    }


    //绑定二维码 "0":地点 "1":对象
    public static void barcode(String url,String type,Long obId){
        QrCode qrCode = QrCode.findByUrl(url);
        if("0".equals(type)){
            Area area  = Area.findById(obId);
            qrCode.editArea(area);
        }
        if("1".equals(type)){
            Goods goods = Goods.findById(obId);
            qrCode.editGoods(goods);
        }
        renderJSON(Result.succeed());
    }

    //扫码
    public static void  scan(@As String code){
        QrCode qrCode = QrCode.findByCode(code);
        if (qrCode !=null){
            if (StringUtils.isBlank(qrCode.type)){
                renderJSON(Result.failed(Result.StatusCode.NOT_USED_QRCODE_URL));
            }else{
                //地点二维码
                if ("0".equals(qrCode.type)){
                    renderJSON(Result.succeed(new AreaVO(qrCode.area),qrCode.type));
                }
                //对象二维码
                if ("1".equals(qrCode.type)){
                    GoodsVO goodsVO = new GoodsVO(qrCode.goods);
                    if(StringUtils.isNotBlank(goodsVO.inspectStatus) && (StringUtils.equals(goodsVO.inspectStatus,"待巡检")
                    || StringUtils.equals(goodsVO.inspectStatus,"未巡检"))){
                        renderJSON(Result.succeed(new GoodsVO(qrCode.goods),qrCode.type));
                    }
                    renderJSON(Result.failed(Result.StatusCode.GOOD_HAD_INSPECT));

                }
            }
        }else {

            renderJSON(Result.failed(Result.StatusCode.NOT_VALID_QRCODE_URL));
        }
    }

    //判断地点合理性
    public static void validArea(long areaId){
        Area area = Area.findById(areaId);
        Person person = CommonUtils.currPerson();
        List<Area> areas = GoodsLog.findByChecker(person).stream()
                                    .map(gl -> gl.goods.area).collect(Collectors.toList());
        if(areas.contains(area)){
            renderJSON(Result.succeed());
        }else{
            renderJSON(Result.failed(Result.StatusCode.NOT_VALID_AREA));
        }

    }

    //获取地点信息
    public static void areaInfo(long areaId){
        Area area = Area.findById(areaId);
        AreaVO vo = new AreaVO(area);
        vo.isValid = false;
        renderJSON(Result.succeed(vo));
    }


    //个人中心
    public static void personCenter(){
        Person person = CommonUtils.currPerson();
        PersonVO personVO = new PersonVO(person);
        renderJSON(Result.succeed(personVO));

    }

    //信息中心
    public static void infoCenter(){
        Person person = CommonUtils.currPerson();
        if(person == null){
            System.err.println("人员为空");
        }
        OrganizeGroupPerson ogp = OrganizeGroupPerson.findByPerson(person);
        List<Notice> notices = new ArrayList<>();
        if(ogp.group.isPlat){
            PersonRole personRole = PersonRole.findByPerson(person);
            notices = Notice.fetchByRegion(personRole.role.region,1,Integer.MAX_VALUE);
        }else {
            notices = Notice.fetchByGroup(ogp.group, 1, Integer.MAX_VALUE);
        }

        List<NoticeVO> noticeVOS = NoticeVO.toList(notices);
        renderJSON(noticeVOS);
    }


    //公司平面图
    public static void orgInfo(){
        Person person = CommonUtils.currPerson();
        if (person == null){
            System.err.println("人员为空");
        }
        OrganizeGroupPerson ogp = OrganizeGroupPerson.findByPerson(person);
        OrganizeGroup group = ogp.group;
        OrganizeGroupVO vo = new OrganizeGroupVO(group);
        PersonRole personRole = PersonRole.findByPerson(person);
        if(personRole != null && personRole.role != null){
            vo.curRegionLevel = personRole.role.region == null?"":personRole.role.region.level;
            vo.curRegionId = personRole.role.region == null?null:personRole.role.region.id;
            vo.curRegionName = personRole.role.region == null?"":personRole.role.region.name;
        }
        renderJSON(Result.succeed(vo));
    }

    public static void getOrgInfo(long orgId){
        OrganizeGroup group = OrganizeGroup.findById(orgId);
        OrganizeGroupVO vo = new OrganizeGroupVO(group);
        renderJSON(Result.succeed(vo));
    }


    //获取巡检对象的非正常选项
    public static void goodsAbnormals(Long goodsId){
        List<Abnormal> abnormals = new ArrayList<>();
        if(goodsId != null) {
            Goods goods = Goods.findById(goodsId);
            if (StringUtils.isNotBlank(goods.abnormalIds)) {
                String[] abIds = goods.abnormalIds.split(",");
                for (String abId : abIds) {
                    Abnormal abnormal = Abnormal.findById(Long.valueOf(abId));
                    abnormals.add(abnormal);
                }
            }
        }
        List<AbnormalVO> abnormalVOS = AbnormalVO.tolist(abnormals);
        renderJSON(abnormalVOS);
    }





}
