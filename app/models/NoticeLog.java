package models;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class NoticeLog extends BaseModel{


    @ManyToOne
    public Notice notice;

    @ManyToOne
    public Person person;



    public static NoticeLog add(Notice notice,Person person){
        NoticeLog noticeLog = new NoticeLog();
        noticeLog.notice = notice;
        noticeLog.person = person;
        return noticeLog.save();
    }


    public static NoticeLog findLog(Notice notice,Person person){
       return NoticeLog.find(getDefaultContitionSql(" notice = ? and person = ? "),notice,person).first();
    }






}
