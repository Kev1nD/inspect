package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/7/12.
 */

@Entity
public class OrganizeGroupArea extends BaseModel{

    @ManyToOne
    public OrganizeGroup group;


    @ManyToOne
    public Area area;


    public static OrganizeGroupArea add(OrganizeGroup group,Area area){
        OrganizeGroupArea oga = new OrganizeGroupArea();
        oga.group = group;
        oga.area  = area;
        return oga.save();
    }




    public static OrganizeGroup findByArea(Area area){
        return find("select ogg.group from OrganizeGroupArea ogg where ogg.isDeleted = 0 and" +
                "   ogg.group.isDeleted = 0 and ogg.area.isDeleted = 0 and ogg.area = ? ",area).first();
    }

    public static List<Area> findByGroup(OrganizeGroup group){
        return find("select oga.area from OrganizeGroupArea oga where oga.isDeleted = 0 and" +
                "   oga.group.isDeleted = 0 and oga.area.isDeleted = 0 and oga.group = ? ",group).fetch();
    }


    public static List<Area> fetchAll(){
        return find("select oga.area from OrganizeGroupArea oga where oga.isDeleted = 0 and" +
                "   oga.group.isDeleted = 0 and oga.area.isDeleted = 0 ").fetch();
    }

    public static List<OrganizeGroupArea> findByGroups(List<OrganizeGroup> groups){
        if (groups.isEmpty()){
            return Collections.emptyList();
        }

        return find("select ogg from OrganizeGroupArea ogg where ogg.isDeleted = 0 and ogg.group in(:groups)").bind("groups",groups.toArray()).fetch();
    }



    public static List<Area> findByCondition(OrganizeGroup group,List<OrganizeGroup> orgs,Long areaId,Long checkerId){
        StringBuffer sb = new StringBuffer();
        sb.append("select ogg.area from OrganizeGroupArea ogg where ogg.isDeleted = 0 and ogg.area.isDeleted = 0 and ogg.group.isDeleted = 0 ");
        if (group.isPlat){
            if (orgs == null){

            }else{
                sb.append(" and ogg.group.id in (");
                for (int i = 0;i<orgs.size();i++){
                    Long orgId = orgs.get(i).id;
                    sb.append("'").append(orgId).append("'");
                    if (i <orgs.size() -1){
                        sb.append(",");
                    }
                }
                sb.append(")");

            }
        }else{
            sb.append(" and ogg.group.id = ").append("'").append(group.id).append("'");

        }
        if (areaId != null){
            sb.append(" and ogg.area.id = ").append("'").append(areaId).append("'");
        }
        if(checkerId != null){
            sb.append(" and ogg.area.checker.id = ").append("'").append(checkerId).append("'");
        }
        return find(sb.toString()).fetch();
    }


}
