package models;

import org.apache.commons.lang.BooleanUtils;

import javax.persistence.Entity;
import javax.persistence.Lob;
import java.util.List;

/**
 * Created by daikai on 2017/8/26.
 */


@Entity
public class News extends BaseModel{

    @Lob
    public String title;

    @Lob
    public String content;

    @Deprecated
    public Boolean isNews;//true:新闻;false:广告


    public String type;//"0":新闻;"1":广告;"2":知识科普


    public String imgUrl;


    public static News add(String title,String content,String imgUrl,String type){
        News news = new News();
        news.title = title;
        news.content = content;
        news.imgUrl = imgUrl;
        news.type = type;
        return news.save();
    }


    public void edit(String title,String content,String imgUrl,String type){
        this.title = title;
        this.content = content;
        this.imgUrl = imgUrl;
        this.type = type;
        this.save();
    }


    public static List<News> fetchAll(){
        return find("isDeleted = 0 ").fetch();
    }


    public static List<News> fetchByType(String type,int page,int pageSize){
        return find("isDeleted = 0 and type = ? order by createTime desc ",type).fetch(page,pageSize);
    }




}
