package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Collections;
import java.util.List;

/**
 *
 * 用户角色关联表
 * Created by daikai on 2017/7/8.
 */


@Entity
public class PersonRole extends BaseModel{

    @ManyToOne
    public Role role;

    @ManyToOne
    public Person person;


    public static PersonRole add(Role role,Person person){
        PersonRole personRole = new PersonRole();
        personRole.role = role;
        personRole.person = person;
        return  personRole.save();
    }

    public void editRole(Role role){
        this.role = role;
        this.save();
    }


    public static PersonRole findByPerson(Person person){
        return find(getDefaultContitionSql(" person = ? and person.isDeleted = 0 "),person).first();
    }


    public static List<PersonRole> fetchByRoleRegion(OrganizeGroup group,List<Region> regions){
        if(regions.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        return find(" select pr from PersonRole pr where pr.isDeleted = 0 and pr.role.isDeleted = 0 " +
                "   and pr.person.isDeleted = 0 and pr.role.group = ? and pr.role.region in (:regions)",group)
                    .bind("regions",regions.toArray()).fetch();
    }


    public static PersonRole findByRole(Role role){
        return find(getDefaultContitionSql("role = ? "),role).first();
    }
}
