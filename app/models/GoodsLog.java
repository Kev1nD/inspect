package models;

import models.enums.GoodStatus;
import models.enums.InspectStatus;
import models.enums.RecordStatus;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

/**
 * 巡检记录
 * Created by daikai on 2017/7/13.
 */

@Entity
public class GoodsLog extends BaseModel{

    @ManyToOne
    public Goods goods;


    @ManyToOne
    public OrganizeGroup group;


    @Enumerated(EnumType.STRING)
    public InspectStatus status = InspectStatus.待巡检;

    @Lob
    public String imgUrls;


    public Long checkTime;//检查时间

    @Lob
    public String reason;//不正常理由

    @Enumerated(EnumType.STRING)
    public RecordStatus rstatus;


    public String videoUrl;



    public static  GoodsLog add(Goods goods,OrganizeGroup group){
        GoodsLog goodsLog = new GoodsLog();
        goodsLog.goods = goods;
        goodsLog.group = group;
        return goodsLog.save();

    }


    public void editGoodsLog(String imgUrls,String videoUrl,InspectStatus status,String reason){
        this.imgUrls = imgUrls;
        this.videoUrl = videoUrl;

        if(this.rstatus != null && this.rstatus == RecordStatus.未通过){
            this.rstatus = RecordStatus.二次巡检;
        }else{
            if(this.status == InspectStatus.未巡检){
                this.rstatus = RecordStatus.过期巡检;
            }else {
                this.rstatus = RecordStatus.待审核;
            }
        }
        this.status = status;
        this.checkTime = System.currentTimeMillis();
        if(status == InspectStatus.非正常){
            this.reason = reason;
            //同步到goods
            this.goods.editStatus(GoodStatus.非正常);
            this.goods.setIsNeedRepair(true);
        }

        this.save();
    }


    public void editIStatus(InspectStatus status){
        this.status = status;
        this.reason = null;
        this.save();
    }

    public void setRstatus(RecordStatus status){
        this.rstatus = status;
        this.save();
    }

    public static List<GoodsLog> fetchAll(){
        return find("select gl from GoodsLog gl where gl.isDeleted = 0 and gl.goods.isDeleted = 0 and gl.group.isDeleted = 0 order by gl.createTime desc").fetch();
    }

    public static GoodsLog findByGoods(Goods goods){
        return find(getDefaultContitionSql("goods =? "),goods).first();
    }

    public static List<GoodsLog> findLogsByGoods(Goods goods){
        return find(getDefaultContitionSql("goods =? "),goods).fetch();
    }

    public static List<GoodsLog> findByGroup(OrganizeGroup group){
        return find(getDefaultContitionSql(" group = ? and group.isDeleted = 0 order by createTime desc"),group).fetch();
    }

    public static List<GoodsLog> findByGroups(List<OrganizeGroup> groups){
        if(groups.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        return find(getDefaultContitionSql(" group in(:groups) order by createTime desc")).bind("groups",groups.toArray()).fetch();
    }


    public static List<GoodsLog> findByChecker(Person person,InspectStatus status){
        return find("select gl from GoodsLog gl where gl.goods.area.checker = ? " +
                "and gl.status = ? and gl.isDeleted = 0 order by gl.createTime desc ",person,status).fetch();
    }

    /**
     * @Description:app中未巡检的也需要出现在列表
     * @Date: 下午9:26 2017/9/26
     */
    public static List<GoodsLog> findByChecker(Person person){
        return find("select gl from GoodsLog gl where gl.goods.area.checker = ? " +
                "and gl.status in('未巡检','待巡检') and gl.isDeleted = 0 order by gl.createTime desc ",person).fetch();
    }


    public static List<GoodsLog> findByStatus(InspectStatus status){
        return find("select gl from GoodsLog gl where   gl.status = ? " +
                "   and gl.isDeleted = 0 and gl.group.isDeleted = 0 and gl.goods.isDeleted = 0 ",status).fetch();
    }

    public static List<GoodsLog> findByGroupAndStatus(OrganizeGroup group,InspectStatus status){
        return find("select gl from GoodsLog gl where   gl.status = ? and gl.group = ? " +
                "   and gl.isDeleted = 0 and gl.group.isDeleted = 0 and gl.goods.isDeleted = 0 ",status,group).fetch();
    }

    public static List<GoodsLog> findAbnormalByGroupAndStatus(OrganizeGroup group,InspectStatus status){
        return find("select gl from GoodsLog gl where   gl.status = ? and gl.group = ? " +
                "   and gl.isDeleted = 0 and gl.group.isDeleted = 0 and gl.goods.isDeleted = 0 and gl.goods.isNeedRepair = 1  ",status,group).fetch();
    }


    public static List<GoodsLog> findByCheckerAndArea(Person person,List<Goods> checkgoods,InspectStatus status){
        return find("select gl from GoodsLog gl where gl.goods.area.checker = ? " +
                "and gl.status = ? and gl.isDeleted = 0 and gl.goods in (:checkgoods) order by gl.createTime desc",person,status).bind("checkgoods",checkgoods.toArray()).fetch();
    }


    public static List<GoodsLog> findByPerson(Person person){
        return find("select gl from GoodsLog gl where gl.goods.area.checker = ? " +
                " and gl.isDeleted = 0 order by gl.createTime desc ",person).fetch();
    }

    //获取上次巡检的记录
    public static GoodsLog findCheckedLogByPerson(Person person){
        return find("select gl from GoodsLog gl where gl.goods.area.checker = ? " +
                " and gl.isDeleted = 0 and gl.checkTime is not null order by gl.checkTime desc ",person).first();
    }


    //检查点应有的对象数量
    public static Long countGoodsNumByArea(Area area){
        return GoodsLog.find("select  count(distinct ogg.goods) from GoodsLog ogg where ogg.goods.isDeleted = 0 " +
                " and ogg.group.isDeleted = 0 and ogg.goods.area = ? and ogg.status in ('未巡检','待巡检') and ogg.isDeleted = 0 ",area).first();

    }

    public static Long countGoodsNumByAreaAndName(Area area,String goodsName){
        return GoodsLog.find("select  count(distinct ogg.goods) from GoodsLog ogg where ogg.goods.isDeleted = 0 " +
                " and ogg.group.isDeleted = 0 and ogg.goods.area = ? and ogg.goods.name = ?  and ogg.status in ('未巡检','待巡检') and ogg.isDeleted = 0 ",area,goodsName).first();

    }

    //根据机构和状态获取goods数量
    public static Long countGoodsByGroupsAndStatus(List<OrganizeGroup> groups, InspectStatus status){
        if(groups.isEmpty()){
            return 0l;
        }
        return GoodsLog.find("select  count(distinct ogg.goods) from GoodsLog ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.status = ? and ogg.group in (:groups)  order by ogg.createTime desc ",status).bind("groups",groups.toArray()).first();
    }



    //根据机构和状态获取goods数量
    public static Long countGoodsByGroupAndStatus(OrganizeGroup group, InspectStatus status,long startTime,long endTime){

        return GoodsLog.find("select  count(distinct ogg.goods) from GoodsLog ogg where ogg.goods.isDeleted = 0" +
                "  and ogg.goods.isDeleted = 0  and ogg.group.isDeleted = 0 and ogg.status = ? and ogg.group =?" +
                "  and ogg.goods.createTime >= ? and ogg.goods.createTime <= ? order by ogg.createTime desc ",status,group,startTime,endTime).first();
    }


    //根据机构和状态获取goods数量
    public static Long countGoodsByGroupsAndTimeStatus(List<OrganizeGroup> groups, InspectStatus status,long startTime,long endTime){
        if(groups.isEmpty()){
            return 0l;
        }
        return GoodsLog.find("select  count(distinct ogg.goods) from GoodsLog ogg where ogg.goods.isDeleted = 0" +
                "  and ogg.goods.isDeleted = 0  and ogg.group.isDeleted = 0 and ogg.status = ? " +
                "  and ogg.goods.createTime >= ? and ogg.goods.createTime <= ? and ogg.group in (:groups) order by ogg.createTime desc ",status,startTime,endTime).bind("groups",groups.toArray()).first();
    }




    public static List<GoodsLog> findByCondition(OrganizeGroup group,List<OrganizeGroup> orgs,Long goodsId,Long areaId){
        StringBuffer sb = new StringBuffer();
        sb.append("select gl from GoodsLog gl where gl.isDeleted = 0 and gl.goods.isDeleted = 0 and gl.group.isDeleted = 0 ");
        if (group.isPlat){
            if (orgs == null){

            }else{
                sb.append(" and gl.group.id in (");
                for (int i = 0;i<orgs.size();i++){
                    Long orgId = orgs.get(i).id;
                    sb.append("'").append(orgId).append("'");
                    if (i <orgs.size() -1){
                        sb.append(",");
                    }
                }
                sb.append(")");

            }
        }else{
            sb.append(" and gl.group.id = ").append("'").append(group.id).append("'");

        }
        if (areaId != null){
            sb.append(" and gl.goods.area.id = ").append("'").append(areaId).append("'");
        }

        if (goodsId != null){
            sb.append(" and gl.goods.id = ").append("'").append(goodsId).append("'");
        }

        sb.append(" order by gl.createTime desc");
        return find(sb.toString()).fetch();

    }

}
