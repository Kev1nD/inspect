package models;

import org.apache.commons.codec.digest.DigestUtils;
import utils.CommonUtils;
import vos.PersonVO;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by daikai on 2017/6/27.
 */

@Entity
public class Person extends BaseModel{


    public String username;//登录账号

    public String name;

    public String password;

    public String sex;

    public String avator;//头像

    public String job;

    public String mobile;

    public String contact;

    public String officedate;//任职时间

    public String leavedate;//离职时间

    public String company;

    @Lob
    public String description;//岗位描述

    @Lob
    public String remark;

    @ManyToOne
    public Person fzPerson;

    @Lob
    public String resources;//用户权限集合,逗号分割



    public Boolean isLeave = false;//是否离职


    public void editResource(String resources){
        this.resources = resources;
        this.save();
    }


    public void setLeave(){
        this.isLeave = true;
        this.leavedate = CommonUtils.formatDate(System.currentTimeMillis(),"yyyy-MM-dd");
        this.save();
    }


    public static Person add(PersonVO vo){
        Person person = new Person();
        person.username = vo.username;
        person.name = vo.name;
        person.sex = vo.sex;
        person.avator = vo.avator;
        person.job = vo.job;
        if ("巡检员".equals(vo.job) && vo.fuzePersonId != null){
            person.fzPerson = Person.findById(vo.fuzePersonId);
        }
        person.mobile = vo.mobile;
        person.contact = vo.contact;
        person.officedate = vo.officedate;
        person.company = vo.company;
        person.leavedate = vo.leavedate;
        person.description = vo.description;
        person.remark = vo.remark;
        person.password = DigestUtils.md5Hex(vo.password);
        person.save();
        return  person;
    }


    //自动注册,初始密码123456
    public static Person regist(String name,String username){
        if(findByUserName(username) != null){
            return null;
        }
        Person person = new Person();
        person.name = name;
        person.username = username;
        person.password = DigestUtils.md5Hex("123456");
        return person.save();
    }

    public void del(){
        logicDelete();
    }

    public void edit(PersonVO vo){
        this.username = vo.username;
        this.name = vo.name;
        this.sex = vo.sex;
        this.avator = vo.avator;
        this.job = vo.job;
        this.mobile = vo.mobile;
        this.contact = vo.contact;
        this.officedate = vo.officedate;
        this.company = vo.company;
        this.description = vo.description;
        this.remark = vo.remark;
        this.password = DigestUtils.md5Hex(vo.password);
        this.save();
    }

    public void editPlatPer(String name,String mobile,String username){
        this.name = name;
        this.mobile = mobile;
        this.username = username;
        this.save();
    }

    public void editPasswd(String password){
        this.password = DigestUtils.md5Hex(password);
        this.save();
    }


    public static List<Person> fetchAll(){
        return find(getDefaultContitionSql(" 1=1 ")).fetch();
    }


    public static Person findByName(String mobile){
        return find(getDefaultContitionSql(" mobile = ? "),mobile).first();
    }


    public static Person findByUserName(String username){
        return find(getDefaultContitionSql(" username = ? "),username).first();
    }

}
