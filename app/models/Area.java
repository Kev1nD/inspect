package models;

import vos.AreaVO;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 巡检区域
 * Created by daikai on 2017/6/28.
 */

@Entity
public class Area extends  BaseModel{

    public String number;

    public String name;

    public String address;

    public String imageUrl;

    @ManyToOne
    public Person checker;//检查人

    @Lob
    public String remark;


    public Integer goodsNum;//实际数量


    public static Area add(AreaVO areaVO){
        if(findByNumber(areaVO.number) != null){
            return null;
        }
        Area area = new Area();
        area.number = areaVO.number;
        area.name = areaVO.name;
        area.address = areaVO.address;
        area.imageUrl = areaVO.imageUrl;
        area.remark = areaVO.remark;
        area.checker = areaVO.checkerId == null?null:Person.findById(areaVO.checkerId);
        return area.save();

    }

    public void edit(AreaVO areaVO){
        this.number = areaVO.number;
        this.name = areaVO.name;
        this.address =areaVO.address;
        this.imageUrl = areaVO.imageUrl;
        this.remark = areaVO.remark;
        this.checker = Person.findById(areaVO.checkerId);
        this.save();
    }

    public void setGoodsNum(Integer num){
        this.goodsNum = num;
        this.save();
    }


    public void del(){
        logicDelete();
    }


    public static Area findByNumber(String number){
        return Area.find(getDefaultContitionSql(" number = ? "),number).first();
    }

    public static List<Area> fetchByChecker(Person checker){
        return find(getDefaultContitionSql(" checker = ? and checker.isDeleted = 0 "),checker).fetch();
    }


    public static List<Area> fetchAll(){
        return find(getDefaultContitionSql( " 1=1 ")).fetch();
    }



    public static Map<Area,List<Goods>> getAGMap(){
        Map<Area,List<Goods>> map = new HashMap<>();
        for(Area area:fetchAll()){
            map.put(area,Goods.findByArea(area));
        }
        return  map;
    }



}
