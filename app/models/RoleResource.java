package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * 角色权限对应关系
 * Created by daikai on 2017/7/8.
 */

@Entity
public class RoleResource extends BaseModel{


    @ManyToOne
    public Role role;

    @ManyToOne
    public Resource resource;


    public static RoleResource add(Role role,Resource resource){
        RoleResource roleResource = new RoleResource();
        roleResource.role = role;
        roleResource.resource = resource;
        return roleResource.save();
    }


    public static List<Resource> findByRole(Role role){
        return find("select r.resource from RoleResource r where r.isDeleted = 0 and " +
                " r.resource.isDeleted = 0 and r.role.isDeleted = 0 and r.role = ?",role).fetch();
    }



    public static List<String> getRoleName( List<Resource>  resources){
        return  resources.stream().map(r -> r.name).collect(Collectors.toList());
    }


}
