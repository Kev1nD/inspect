package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * 楼层分布图
 * Created by daikai on 2017/7/17.
 */

@Entity
public class FloorPics extends BaseModel{

    public String name;

    public String imgUrl;


    public static  FloorPics add(String name,String imgUrl){
        FloorPics  floorPics = new FloorPics();
        floorPics.name = name;
        floorPics.imgUrl = imgUrl;
        return floorPics.save();
    }


}
