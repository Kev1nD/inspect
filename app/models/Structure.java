package models;

import javax.persistence.Entity;
import java.util.List;

/**
 * Created by daikai on 2017/8/12.
 */
@Entity
public class Structure  extends  BaseModel{


    public String name;


    public static Structure add(String name){
        Structure structure = new Structure();
        structure.name = name;
        return structure.save();
    }


    public static List<Structure> fetchAll(){
        return Structure.find("select s from Structure s where s.isDeleted = 0 ").fetch();
    }

}
