package models;

import org.apache.commons.lang.RandomStringUtils;
import play.Play;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 *
 * 二维码
 * Created by daikai on 2017/7/3.
 */

@Entity
public class QrCode extends  BaseModel{

    public String code;

    public String url;

    @ManyToOne
    public Area area;

    @ManyToOne
    public Goods goods;

    public String type;//"0"：巡检地点；"1"：巡检对象


    public static QrCode add(){
        QrCode qrCode = new QrCode();
        qrCode.code = RandomStringUtils.randomNumeric(10)+System.currentTimeMillis();
        qrCode.url = Play.configuration.getProperty("application.baseUrl")+"/app/scan/"+qrCode.code;
        return qrCode.save();
    }

    public void editArea(Area area){
        this.area = area;
        this.type = "0";
        this.save();
    }


    public void editGoods(Goods goods){
        this.goods = goods;
        this.type = "1";
        this.save();
    }

    public static List<QrCode> fetchAll(){
        return find("select q from QrCode q where q.isDeleted = 0 ").fetch();
    }


    public static List<QrCode> fetchBysize(int page,int pageSize){
        return find("select q from QrCode q where q.isDeleted = 0 ").fetch(page,pageSize);
    }

    public static QrCode findByUrl(String url){
        return find(getDefaultContitionSql(" url = ? "),url).first();
    }

    public static QrCode findByCode(String code){
        return  find(getDefaultContitionSql(" code = ? "),code).first();
    }

}
