package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by daikai on 2017/8/12.
 */

@Entity
public class GoodsType extends BaseModel{


    public String name;

    @ManyToOne
    public OrganizeGroup group;



    public static GoodsType add(String name,OrganizeGroup group){
        GoodsType goodsType = new GoodsType();
        goodsType.name = name;
        goodsType.group = group;
        return goodsType.save();

    }



    public static List<GoodsType> fetchByGroup(OrganizeGroup group){
        return GoodsType.find(getDefaultContitionSql(" group = ? "),group).fetch();
    }



}
