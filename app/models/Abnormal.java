package models;

import javax.persistence.Entity;
import java.util.List;

/**
 *
 * 非正常选项
 * Created by daikai on 2017/8/12.
 */
@Entity
public class Abnormal extends  BaseModel{

    public String name;


    public static Abnormal add(String  name){
        Abnormal abnormal = new Abnormal();
        abnormal.name = name;
        return abnormal.save();
    }



    public static List<Abnormal> fetchAll(){
        return  Abnormal.find("select ab from Abnormal ab where ab.isDeleted = 0 ").fetch();
    }


}
