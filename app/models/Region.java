package models;

import utils.AmapUtils;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *省市
 * Created by daikai on 2017/7/4.
 */

@Entity
public class Region extends BaseModel{

    public String citycode;
    public String adcode;
    public String name;
    public String center;
    public String level;// country|province|city|district|biz_area
    @ManyToOne
    public Region parentArea;

    public static final String[] LEVELS = { "country", "province", "city", "district", "biz_area" };

    public static Region add(String citycode, String adcode, String name, String center, String level, Region parentArea) {
        Region area = new Region();
        area.citycode = citycode;
        area.adcode = adcode;
        area.name = name;
        area.center = center;
        area.level = level;
        area.parentArea = parentArea;
        return area.save();
    }

    public static void init() {
        if (!fetchAll().isEmpty()) {
            return;
        }
        AmapUtils.AmapResult amapResult = AmapUtils.getAll();
        for (AmapUtils.AreaResult areaResult : amapResult.districts) {
            Region area = add("", areaResult.adcode, areaResult.name, areaResult.center, areaResult.level, null);
            init(areaResult, area);
        }
    }

    public static void init(AmapUtils.AreaResult areaResult, Region parea) {
        for (AmapUtils.AreaResult careaResult : areaResult.districts) {
            Region area = add("", careaResult.adcode, careaResult.name, careaResult.center, careaResult.level, parea);
            init(careaResult, area);

        }
    }

    public static List<Region> fetchAll() {
        return Region.find("select a from Region a where a.isDeleted=false").fetch();
    }

    public static List<Region> fetchByLevel(String level) {
        return Region.find(getDefaultContitionSql(" level = ? "), level).fetch();
    }

    public static List<Region> fetchByParent(Region parentArea) {
        return Region.find("select a from Region a where a.isDeleted=false and a.parentArea = ? order by a.adcode ", parentArea).fetch();
    }





    public static List<Region> getchildrens(Region region){
        List<Region> childrens = new ArrayList<>();
        if(region != null) {
            if (region.level.equals("street")) {
                childrens.add(region);
            } else {
                List<Region> regions = Region.fetchByParent(region);
                for (Region r : regions) {
                    childrens.add(r);
                    if (!r.level.equals("street")) {
                        childrens.addAll(getchildrens(r));
                    }
                }
            }
        }
        return  childrens;

    }



    public static List<Region> getParents(Region region){
        List<Region> parents = new ArrayList<>();
        if(region != null){
            if(!region.level.equals("country")){
                parents.add(region.parentArea);
                parents.addAll(getParents(region.parentArea));
            }
        }
        return  parents;
    }
}
