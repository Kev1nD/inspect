package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by daikai on 2017/7/17.
 */

@Entity
public class OrganizeGroupPics extends BaseModel {

    @ManyToOne
    public OrganizeGroup group;


    @ManyToOne
    public FloorPics pic;

    public static OrganizeGroupPics add(OrganizeGroup group,FloorPics pic){
        OrganizeGroupPics pics = new OrganizeGroupPics();
        pics.group = group;
        pics.pic = pic;
        return pics.save();
    }


    public static OrganizeGroupPics findByGroupAndPic(OrganizeGroup group,FloorPics pic){
        return  find(getDefaultContitionSql(" group=? and pic = ? "),group,pic).first();
    }

    public static List<FloorPics> fetchByGroup(OrganizeGroup group){
        return OrganizeGroupPics.find("select ogp.pic from OrganizeGroupPics ogp where ogp.isDeleted = 0 and ogp.group.isDeleted = 0" +
                "   and ogp.pic.isDeleted = 0 and ogp.group = ? ",group).fetch();
    }




}
