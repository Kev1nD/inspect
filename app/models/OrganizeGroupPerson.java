package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/7/8.
 */

@Entity
public class OrganizeGroupPerson extends BaseModel{

    @ManyToOne
    public OrganizeGroup group;


    @ManyToOne
    public Person person;



    public static OrganizeGroupPerson add(OrganizeGroup group,Person person){
        OrganizeGroupPerson ogp = new OrganizeGroupPerson();
        ogp.group = group;
        ogp.person = person;
        return ogp.save();
    }


    public static OrganizeGroupPerson findByPerson(Person person){
        return  OrganizeGroupPerson.find(getDefaultContitionSql(" person = ? and group.isDeleted =0 and person.isDeleted = 0 "),person).first();
    }

    public static List<OrganizeGroupPerson> fetchAll(){
        return find("select ogp from OrganizeGroupPerson ogp where ogp.isDeleted = 0 and ogp.group.isDeleted = 0" +
                "   and ogp.person.isDeleted = 0 and ogp.group.isPlat = 0 ").fetch();
    }


    public static List<Person> findPersonbyGroup(OrganizeGroup group){
        return find("select ogp.person from OrganizeGroupPerson ogp where ogp.isDeleted = 0 and ogp.group.isDeleted = 0" +
                "   and ogp.person.isDeleted = 0 and ogp.group = ? ",group).fetch();
    }


    public static List<OrganizeGroupPerson> findOrgPersonbyGroup(OrganizeGroup group){
        return find("select ogp from OrganizeGroupPerson ogp where ogp.isDeleted = 0 and ogp.group.isDeleted = 0" +
                "   and ogp.person.isDeleted = 0 and ogp.group = ? ",group).fetch();
    }


    public static List<OrganizeGroupPerson> findPersonbyGroups(List<OrganizeGroup> groups){
        if(groups.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        return find("select ogp from OrganizeGroupPerson ogp where ogp.isDeleted = 0 and ogp.group.isDeleted = 0" +
                "   and ogp.person.isDeleted = 0 and ogp.group in (:groups)").bind("groups",groups.toArray()).fetch();
    }




}
