package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Collections;
import java.util.List;

/**
 * 角色表
 * Created by daikai on 2017/7/4.
 */

@Entity
public class Role extends BaseModel{

    @ManyToOne
    public OrganizeGroup group;

    public String roleName;//角色名称

    @ManyToOne
    public Region region;//地域选择

    public static Role add(OrganizeGroup group,String roleName,Region region){
        Role role  = new Role();
        role.group =group;
        role.roleName = roleName;
        role.region = region;
        return role.save();
    }


    public static Role findBygroupAndName(OrganizeGroup group,String roleName){
        return find(getDefaultContitionSql(" group = ? and roleName =? "),group,roleName).first();
    }


    public static List<Role> findByGroup(OrganizeGroup group){
        return find(getDefaultContitionSql("group = ? "),group).fetch();
    }

    public static List<Role> findNotAdminByGroup(OrganizeGroup group){
        return find(getDefaultContitionSql("group = ? and roleName != '企业管理员' "),group).fetch();
    }

    public static Role findByGroupAndName(OrganizeGroup group,String roleName){
        return find(getDefaultContitionSql("group = ? and roleName = ? "),group,roleName).first();
    }

    public static List<Role> findByRegions(List<Region> regions){
        if(regions.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        return find(getDefaultContitionSql("region in (:regions)")).bind("regions",regions.toArray()).fetch();
    }

    public static List<Role> fetchAll(){
        return find("select r from Role r where r.isDeleted = 0 and r.group.isDeleted = 0").fetch();
    }
    public static List<Role> fetchAllOrg(){
        return find("select r from Role r where r.isDeleted = 0 and r.group.isDeleted = 0 and r.group.isPlat = 0 ").fetch();
    }

    //谨慎使用
    public static List<Role> findByRegionsAndGroup(List<Region> regions,OrganizeGroup group){
        if(regions.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        return find(getDefaultContitionSql("group = ? and region in (:regions)"),group).bind("regions",regions.toArray()).fetch();
    }



    //注意：企业管理员以为的角色，regionID是空的
    public static List<Role> findOrgRolesByGroups(List<OrganizeGroup> groups){
        if(groups.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        return find(getDefaultContitionSql("group.isPlat = 0 and group in (:groups)")).bind("groups",groups.toArray()).fetch();
    }




}
