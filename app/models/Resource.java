package models;

import javax.persistence.Entity;
import java.util.List;

/**
 *
 * 权限
 * Created by daikai on 2017/7/8.
 */
@Entity
public class Resource extends BaseModel{

    public String name;//权限名称

    public String code;


    public static Resource add(String name,String code){
        Resource resource = new Resource();
        resource.name = name;
        resource.code = code;
        return  resource.save();
    }

    public void edit(String name,String code){
        this.name = name;
        this.code = code;
        this.save();
    }

    public static List<Resource> fetchAll(){
        return find("select r from Resource r where r.isDeleted = 0 ").fetch();
    }

    public static Resource findbycode(String code){
        return find(getDefaultContitionSql("  code = ? "),code).first();
    }

    public static void init(){
        if(fetchAll().isEmpty()){
            Resource.add("首页","v1");
            Resource.add("记录查询","v2");
            Resource.add("对象管理","v3");
            Resource.add("区域点管理","v4");
            Resource.add("人员管理","v5");
            Resource.add("企业信息","v6");
            Resource.add("供应商管理","v7");

            Resource.add("查看","o1");
            Resource.add("编辑","o2");
            Resource.add("添加、删除","o3");
            Resource.add("企业进入","o4");
            Resource.add("审核","o5");
            Resource.add("维修","o6");
            Resource.add("通知公告管理","m1");

            Resource.add("单位巡查对象数量列表","s1");
            Resource.add("单位巡查对象“S”编号列表","s2");
            Resource.add("单位建筑及分布图情况列表","s3");
            Resource.add("单位巡查对象不合格情况列表","s4");
            Resource.add("当月单位不合格数状况列表","s5");
            Resource.add("单位巡查对象不在使用中的数量列表","s6");
            Resource.add("单位巡查对象的生产、使用、检修、报废日期列表","s7");
            Resource.add("单位相关人员列表","s8");
            Resource.add("单位产品、规模列表","s9");
            Resource.add("单位供应商列表","s10");
            Resource.add("单位对象数量列表","s11");
        }


    }


}
