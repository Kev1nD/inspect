package models;

import net.sf.oval.constraint.Max;
import vos.SupplierVO;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 *
 * 供应商
 * Created by daikai on 2017/6/28.
 */
@Entity
public class Supplier extends BaseModel{

    public String type;

    public String name;

    public String address;

    public String contact;

    public String job;

    public String mobile;

    public Boolean isQualified;//是否有资质

    public String licence;


    public String permit;

    @Lob
    public String remark;

    public String registNum;//注册号

    @ManyToOne
    public Region region;


    public String manager;

    public String manageMobile;//总经理电话

    public static Supplier add(SupplierVO vo){
        Supplier supplier = new Supplier();
        supplier.type = vo.type;
        supplier.name = vo.name;
        supplier.address = vo.address;
        supplier.contact = vo.contact;
        supplier.job = vo.job;
        supplier.mobile = vo.mobile;
        supplier.isQualified = vo.isQualified;
        supplier.licence = vo.licence;
        supplier.permit = vo.permit;
        supplier.remark = vo.remark;
        supplier.registNum = vo.registNum;
        supplier.manager = vo.manager;
        supplier.manageMobile = vo.manageMobile;
        if(vo.regionId != null){
            supplier.region  = Region.findById(Long.valueOf(vo.regionId));
        }
        return  supplier.save();

    }


    public void edit(SupplierVO vo){
        this.type = vo.type;
        this.name = vo.name;
        this.address = vo.address;
        this.contact = vo.contact;
        this.job = vo.job;
        this.mobile = vo.mobile;
        this.isQualified = vo.isQualified;
        this.licence = vo.licence;
        this.permit = vo.permit;
        this.remark = vo.remark;
        this.registNum = vo.registNum;
        this.manager = vo.manager;
        this.manageMobile = vo.manageMobile;
        this.save();
    }


    public static List<Supplier> fetchAll(){
        return find("select s from Supplier s where s.isDeleted = 0 ").fetch();
    }
}
