package models;

import javax.persistence.Entity;
import java.util.List;

/**
 * Created by daikai on 2017/9/28.
 */


@Entity
public class GoodsName extends BaseModel{

    public String name;

    public static  GoodsName add(String name){
        GoodsName goodsName = new GoodsName();
        goodsName.name = name;
        return goodsName.save();
    }



    public static List<GoodsName> fetchAll(){
        return  GoodsName.find("select gn from GoodsName gn where gn.isDeleted = 0 ").fetch();
    }

}
