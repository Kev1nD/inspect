package models;

import org.apache.commons.lang.StringUtils;
import utils.CommonUtils;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/8/28.
 */

@Entity
public class Notice extends BaseModel{

    @ManyToOne
    public OrganizeGroup group;

    @ManyToOne
    public Region region;

    @Lob
    public String title;

    @Lob
    public String content;


    public static Notice add(OrganizeGroup group,String title,String content){
        Notice notice = new Notice();
        notice.group = group;
        notice.title = title;
        notice.content = content;
        return notice.save();
    }


    public static Notice add2(OrganizeGroup group,Region region,String title,String content){
        Notice notice = new Notice();
        notice.group = group;
        notice.title = title;
        notice.content = content;
        notice.region = region;
        return notice.save();
    }



    public void edit(String title,String content){
        if(!StringUtils.equals(this.title,title)){
            this.title = title;
        }
        if(!StringUtils.equals(this.content,content)){
            this.content = content;
        }
        this.save();
    }


    public static boolean isRead(Notice notice){
        return NoticeLog.findLog(notice, CommonUtils.currPerson()) != null;
    }

    public static List<Notice> fetchByGroup(OrganizeGroup group,int page,int pageSize){
        return Notice.find(getDefaultContitionSql(" group = ? order by createTime desc"),group).fetch(page,pageSize);
    }



    public static List<Notice> fetchByRegions(List<Region> regions,int page,int pageSize){
        if (regions.isEmpty()){
            return Collections.emptyList();
        }
        return Notice.find(getDefaultContitionSql(" group.id = 1l and region in(:regions) order by createTime desc"))
                .bind("regions",regions.toArray()).fetch(page,pageSize);
    }



    public static List<Notice> fetchByRegion(Region region,int page,int pageSize){
        return Notice.find(getDefaultContitionSql(" group.id = 1l and region = ? order by createTime desc"),region).fetch(page,pageSize);
    }


}
