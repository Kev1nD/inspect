package models.enums;

/**
 *
 * 巡检对象状态
 * Created by daikai on 2017/7/1.
 */
public enum GoodStatus {

    使用中(10),维修中(20),已报废(30),非正常(40);

    private int  code;

    private GoodStatus(int code){
        this.code = code;
    }


    public GoodStatus convert(int code){
        for (GoodStatus goodStatus:GoodStatus.values()){
            if (goodStatus.value() == code){
                return  goodStatus;
            }
        }
        return  null;
    }


    public int value(){
        return  this.code;
    }



}
