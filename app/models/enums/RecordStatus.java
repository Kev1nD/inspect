package models.enums;

/**
 * 巡检记录审核状态
 * Created by daikai on 2017/8/1.
 */
public enum RecordStatus {

    待审核,通过,未通过,二次巡检,过期巡检
}
