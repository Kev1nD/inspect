package models.enums;

/**
 * 巡检记录状态
 * Created by daikai on 2017/7/18.
 */
public enum InspectStatus {

    待巡检,正常,非正常,未巡检

}
