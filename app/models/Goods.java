package models;

import models.enums.GoodStatus;
import org.apache.commons.lang.StringUtils;
import vos.GoodsVO;

import javax.persistence.*;
import java.util.List;

/**
 *
 * 巡检对象
 * Created by daikai on 2017/6/28.
 */

@Entity
public class Goods extends BaseModel{

    @ManyToOne
    public Area area;

    public  String number;

    public String name;

    public String type;

    public String snumber;//检查对象S编号

    @Enumerated(EnumType.STRING)
    public GoodStatus status = GoodStatus.使用中;//检查状态

    public String createdate;//生产日期

    public String usedate;//使用日期


    //字段不使用
    public String repairdate;//维修日期

    public String scrapdate;//报废日期

    public String imgUrl;

    @ManyToOne
    public Supplier producer;//生产单位

    @ManyToOne
    public Supplier supplyer;//生产单位


    @Lob
    public String repairPeriod;//维修周期


    public String cycleTime;//巡检周期（半个月，1个月）


    public String validTime;//巡检有效期（以天数为单位）

    public String abnormalIds;




    //是否需要维修
    public Boolean isNeedRepair = false;

    //上次维修完的时间
    public Long lastRepairedTime;


    //新增巡检对象
    public static Goods add(GoodsVO goodsVO){

        Goods goods = new Goods();
        goods.number = goodsVO.number;
        goods.name = goodsVO.name;
        goods.type = goodsVO.type;
        goods.snumber = goodsVO.snumber;
        goods.imgUrl = goodsVO.imgUrl;
        goods.createdate = goodsVO.createdate;
        goods.usedate = goodsVO.usedate;
        goods.scrapdate  = goodsVO.scrapdate;
        goods.repairPeriod = goodsVO.repairPeriod;
        goods.cycleTime  = goodsVO.cycleTime;
        goods.validTime = goodsVO.validTime;
        goods.area =Area.findById(goodsVO.areaId);
        goods.abnormalIds  = goodsVO.abnormalIds;
        goods.producer =goodsVO.producerId == null?null:Supplier.findById(goodsVO.producerId);
        goods.supplyer =goodsVO.supplyerId == null?null:Supplier.findById(goodsVO.supplyerId);
        return  goods.save();
    }


    public void del(){
        logicDelete();
    }


    public void eidt(GoodsVO vo){

        this.number = vo.number;
        this.name = vo.name;
        this.type = vo.type;
        this.snumber = vo.snumber;
        this.imgUrl = vo.imgUrl;
        this.createdate = vo.createdate;
        this.usedate = vo.usedate;
        this.repairdate = vo.repairdate;
        this.scrapdate = vo.scrapdate;
        this.producer = vo.producerId == null?null:Supplier.findById(vo.producerId);
        this.supplyer =vo.supplyerId == null?null:Supplier.findById(vo.supplyerId);
        this.area = Area.findById(vo.areaId);
        //刷新维修情况
        if(this.status == GoodStatus.维修中 && ("使用中").equals(vo.status)){
            this.isNeedRepair = false;
            this.lastRepairedTime = System.currentTimeMillis();
        }
        this.status = GoodStatus.valueOf(vo.status);
        this.cycleTime = vo.cycleTime;
        this.validTime = vo.validTime;
        this.abnormalIds  = vo.abnormalIds;
        this.repairPeriod = vo.repairPeriod;
        this.save();

    }

    //维修记录
    public void repair(){
        this.status = GoodStatus.维修中;
        this.save();
    }


    public void editStatus(GoodStatus status){
        this.status = status;
        this.save();
    }



    public void setIsNeedRepair(boolean state){
        this.isNeedRepair = state;
        this.save();
    }

    public static List<Goods> fetchAll(){
        return  Goods.find(getDefaultContitionSql("")).fetch();
    }


    public static Long countGoodsByArea(Area area){
        return Goods.count("select count(g) from Goods g where g.isDeleted = 0 and g.area = ? ",area);
    }

    public static List<Goods> findByArea(Area area){
        return Goods.find(getDefaultContitionSql("area = ? and area.isDeleted = 0 "),area).fetch();
    }

    public static Long countGoodsByAreaAndGoodsName(Area area,String goodsName){
        return Goods.count("select count(g) from Goods g where g.isDeleted = 0 and g.area = ? and g.name = ?",area,goodsName);
    }





}
