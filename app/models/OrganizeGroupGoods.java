package models;

import models.enums.GoodStatus;
import utils.CommonUtils;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by daikai on 2017/7/12.
 */

@Entity
public class OrganizeGroupGoods extends BaseModel{


    @ManyToOne
    public OrganizeGroup group;

    @ManyToOne
    public Goods goods;

    public static OrganizeGroupGoods add(OrganizeGroup group,Goods goods){
        OrganizeGroupGoods ogg = new OrganizeGroupGoods();
        ogg.group = group;
        ogg.goods = goods;
        return ogg.save();
    }


    public static List<Goods> findByGroup(OrganizeGroup group){
        return find("select ogg.goods from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and" +
                "   ogg.group.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group = ? ",group).fetch();
    }

    public static List<Goods> findByGroupNameAndType(OrganizeGroup group){
        return find("select ogg.goods from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and" +
                "   ogg.group.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group = ? group by ogg.goods.name, ogg.goods.type",group).fetch();
    }

    public static Long countByGroupNameAndType(OrganizeGroup group,String name,String type){
        return find("select count(ogg) from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and" +
                "   ogg.group.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group = ? " +
                " and ogg.goods.name = ? and ogg.goods.type = ? ",group,name,type).first();
    }


    public static OrganizeGroup findByGoods(Goods goods){
        return find("select ogg.group from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and" +
                "   ogg.group.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.goods = ? ",goods).first();
    }


    public static List<OrganizeGroupGoods> fetchAll(){
        return find("select ogg from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group.isDeleted = 0").fetch();
    }

    public static List<OrganizeGroupGoods> fetchAllForStatist(List<OrganizeGroup> groups){
        if (groups.isEmpty()){
            return Collections.emptyList();
        }
        return  find("select ogg from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and" +
                "   ogg.group.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group in(:groups) group by ogg.goods.name, ogg.goods.type").bind("groups",groups.toArray()).fetch();
    }


    public static List<OrganizeGroupGoods> fetchByStatus(GoodStatus status){
        return find("select ogg from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group.isDeleted = 0 " +
                " and ogg.goods.status = ? order by ogg.group",status).fetch();
    }


    public static List<OrganizeGroupGoods> fetchByOrgsAndStatus(GoodStatus status,List<OrganizeGroup> groups){
        if (groups.isEmpty()){
            return Collections.emptyList();
        }
        return find("select ogg from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group.isDeleted = 0 " +
                " and ogg.goods.status = ? and ogg.group in(:groups) order by ogg.group",status).bind("groups",groups.toArray()).fetch();
    }


    public static List<OrganizeGroupGoods> findByGroups(List<OrganizeGroup> groups){
        if (groups.isEmpty()){
            return Collections.emptyList();
        }

        return find("select ogg from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and ogg.group in(:groups)").bind("groups",groups.toArray()).fetch();
    }


    public static List<OrganizeGroupGoods> fetchForCheck(){
        return find("select ogg from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and ogg.goods.isDeleted = 0 " +
                "   and ogg.group.isDeleted = 0 and ogg.goods.usedate is not null and ogg.goods.cycleTime is not null and" +
                "   ogg.goods.validTime is not null and ogg.goods.status = '使用中' ").fetch();
    }

    /**
     * @Description:到了维修时间没有维修
     * @Date: 下午2:34 2017/9/28
     */
    public static List<Goods> findNoRepairGoodsByGroup(OrganizeGroup group){

        return OrganizeGroupGoods.find("select distinct(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.status = '使用中' and ogg.goods.isNeedRepair = 1 and ogg.group =? ",group).fetch();
    }


    public static List<Goods> findScrapGoodsByGroup(OrganizeGroup group, String nowTimeStr){

        return OrganizeGroupGoods.find("select distinct(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.scrapdate < ? and ogg.group = ?  ",nowTimeStr,group).fetch();
    }




    //根据机构获取goods数量
    public static Long countGoodsByGroups(List<OrganizeGroup> groups){
        if(groups.isEmpty()){
            return 0l;
        }
        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.group in (:groups)").bind("groups",groups.toArray()).first();
    }


    //根据机构获取goods数量
    public static Long countGoodsByGroupsAndStatus(List<OrganizeGroup> groups, GoodStatus status){
        if(groups.isEmpty()){
            return 0l;
        }
        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.status = ? and ogg.group in (:groups) ",status).bind("groups",groups.toArray()).first();
    }


    //根据机构获取待报废goods数量
    public static Long countBFGoodsByGroupsAndTime(List<OrganizeGroup> groups, String nowTimeStr){
        if(groups.isEmpty()){
            return 0l;
        }
        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.scrapdate <  ? and ogg.group in (:groups) and ogg.goods.status <> '已报废' ",nowTimeStr).bind("groups",groups.toArray()).first();
    }



    //根据机构获取goods数量
    public static Long countGoodsByGroupAndStatus(OrganizeGroup group, GoodStatus status){

        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.status = ? and ogg.group = ?  ",status,group).first();
    }

    public static Long countGoodsByGroupAndStatus(OrganizeGroup group, GoodStatus status,Goods goods){

        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.status = ? and ogg.group = ? and ogg.goods = ? ",status,group,goods).first();
    }


    /**
     * 过了报废日期的
     */
    public static Long countScrapGoodsByGroup(OrganizeGroup group, String nowTimeStr){

        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.scrapdate < ? and ogg.group = ?  ",nowTimeStr,group).first();
    }


    //待维修需要跑批的数据
    public static List<OrganizeGroupGoods> fetchForRepair(){
        return find("select ogg from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and ogg.goods.isDeleted = 0 " +
                "   and ogg.group.isDeleted = 0 and ogg.goods.isNeedRepair  = 0 and ogg.goods.usedate is not null and ogg.goods.status = '使用中' ").fetch();
    }


    //过了时间没有维修的数量
    public static Long countNoRepairGoodsByGroups(List<OrganizeGroup> groups){
        if(groups.isEmpty()){
            return 0l;
        }
        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.status = '使用中' and ogg.goods.isNeedRepair = 1 and ogg.group in (:groups) ").bind("groups",groups.toArray()).first();
    }

    public static Long countNoRepairGoodsByGroup(OrganizeGroup group){

        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.status = '使用中' and ogg.goods.isNeedRepair = 1 and ogg.group =? ",group).first();
    }



    //****************按月份统计*******************

    public static Long countGoodsByGroupAndTime(OrganizeGroup group,Long startTime,Long endTime){
        return find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and" +
                "   ogg.group.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group = ? and " +
                " ogg.goods.createTime >= ? and ogg.goods.createTime <= ? ",group,startTime,endTime).first();
    }


    //根据机构获取goods数量
    public static Long countGoodsByGroupAndStatusAndTime(OrganizeGroup group, GoodStatus status,Long startTime,Long endTime){

        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.status = ? and ogg.group = ? and " +
                "   ogg.goods.createTime >= ? and ogg.goods.createTime <= ?  ",status,group,startTime,endTime).first();
    }


    //根据机构获取goods数量
    public static Long countGoodsByGroupsAndStatusAndTime(List<OrganizeGroup> groups, GoodStatus status,Long startTime,Long endTime){
        if(groups.isEmpty()){
            return 0l;
        }
        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.status = ? and " +
                "   ogg.goods.createTime >= ? and ogg.goods.createTime <= ?  and ogg.group  in(:groups)  ",status,startTime,endTime).bind("groups",groups.toArray()).first();
    }

    public static Long countScrapGoodsByGroupAndTime(OrganizeGroup group, String startTime,String endTime){

        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.scrapdate <= ? and ogg.goods.scrapdate >= ? and ogg.group = ? and ogg.goods.status != '已报废' ",endTime,startTime,group).first();
    }

    public static Long countScrapGoodsByGroupsAndTime(List<OrganizeGroup> groups, String startTime,String endTime){
        if(groups.isEmpty()){
            return 0l;
        }
        return OrganizeGroupGoods.find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.goods.isDeleted = 0" +
                "   and ogg.group.isDeleted = 0 and ogg.goods.scrapdate <= ? and ogg.goods.scrapdate >= ? and ogg.group in(:groups)  and ogg.goods.status != '已报废' ",endTime,startTime).bind("groups",groups.toArray()).first();
    }


    public static Long countGoodsByGroupsAndTime(List<OrganizeGroup> groups,Long startTime,Long endTime){
        if(groups.isEmpty()){
            return 0l;
        }
        return find("select count(ogg.goods) from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and" +
                "   ogg.group.isDeleted = 0 and ogg.goods.isDeleted = 0  and " +
                " ogg.goods.createTime >= ? and ogg.goods.createTime <= ? and ogg.group in(:groups) ",startTime,endTime)
                .bind("groups",groups.toArray()).first();
    }



    public static List<Goods> findByCondition(OrganizeGroup group,List<OrganizeGroup> orgs,Long goodsId){
        StringBuffer sb = new StringBuffer();
        sb.append("select ogg.goods from OrganizeGroupGoods ogg where ogg.isDeleted = 0 and ogg.goods.isDeleted = 0 and ogg.group.isDeleted = 0 ");
        if (group.isPlat){
            if (orgs == null){

            }else{
                sb.append(" and ogg.group.id in (");
                for (int i = 0;i<orgs.size();i++){
                    Long orgId = orgs.get(i).id;
                    sb.append("'").append(orgId).append("'");
                    if (i <orgs.size() -1){
                        sb.append(",");
                    }
                }
                sb.append(")");

            }
        }else{
            sb.append(" and ogg.group.id = ").append("'").append(group.id).append("'");

        }
        if (goodsId != null){
            sb.append(" and ogg.goods.id = ").append("'").append(goodsId).append("'");
        }
        return find(sb.toString()).fetch();
    }


}
