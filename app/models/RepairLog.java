package models;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 *维修记录
 * Created by daikai on 2017/7/13.
 */


@Entity
public class RepairLog extends BaseModel{

    @ManyToOne
    public Goods goods;


    @ManyToOne
    public Supplier supplier;


    @Lob
    public String content;


    public static RepairLog add(Goods goods,Supplier supplier,String content){
        RepairLog repairLog = new RepairLog();
        repairLog.goods = goods;
        repairLog.supplier = supplier;
        repairLog.content = content;
        return repairLog.save();
    }


    public static List<RepairLog> fetchByGoods(Goods goods){
        return find(getDefaultContitionSql(" goods = ? order by createTime desc "),goods).fetch();
    }

    public static RepairLog findByGoods(Goods goods){
        return find(getDefaultContitionSql(" goods = ? "),goods).first();
    }


    public static Long countByGoods(Goods goods){
        return count(getDefaultContitionSql(" goods =? "));
    }

}
