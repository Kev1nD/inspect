package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by daikai on 2017/7/11.
 */

@Entity
public class OrganizeGroupSupplier extends BaseModel{

    @ManyToOne
    public OrganizeGroup group;


    @ManyToOne
    public Supplier supplier;


    public static OrganizeGroupSupplier add(OrganizeGroup group,Supplier supplier){
        OrganizeGroupSupplier ogs = new OrganizeGroupSupplier();
        ogs.group = group;
        ogs.supplier = supplier;
        return ogs.save();
    }

    public static List<Supplier> findByGroup(OrganizeGroup group){
        if(group.isPlat){
            return  find("select ogs.supplier from OrganizeGroupSupplier ogs where ogs.isDeleted = 0 " +
                    "   and ogs.group.isDeleted = 0 and ogs.supplier.isDeleted = 0 ").fetch();
        }
        return find("select ogs.supplier from OrganizeGroupSupplier ogs where ogs.isDeleted = 0 " +
                "   and ogs.group.isDeleted = 0 and ogs.supplier.isDeleted = 0 and ogs.group = ? ",group).fetch();
    }


    public static List<OrganizeGroupSupplier> findAllByGroup(OrganizeGroup group){
        if(group.isPlat){
            return  find("select ogs from OrganizeGroupSupplier ogs where ogs.isDeleted = 0 " +
                    "   and ogs.group.isDeleted = 0 and ogs.supplier.isDeleted = 0 ").fetch();
        }
        return find("select ogs from OrganizeGroupSupplier ogs where ogs.isDeleted = 0 " +
                "   and ogs.group.isDeleted = 0 and ogs.supplier.isDeleted = 0 and ogs.group = ? ",group).fetch();
    }



}
