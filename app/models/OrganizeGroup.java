package models;

import org.apache.commons.lang.StringUtils;
import vos.OrganizeGroupVO;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * 企业实体类
 * Created by daikai on 2017/7/8.
 */
@Entity
public class OrganizeGroup extends BaseModel{

    public Boolean isPlat = false;//是否是平台

    public String name;

    public String type;//以逗号隔开

    public String buildTime;//建筑时间

    public String structure;//建筑结构

    @ManyToOne
    public Region region;//地理位置

    public String estate;//物业

    public String area;//经营范围

    public String product;//生产产品

    public String scale;//生产规模

    public String personNum;//总人数

    public String site;//网址

    public String intro;//简介

    public String licence;//营业执照

    public String permit;//营业许可证

    public String districtUrl;//总分布图

    @Lob
    public String survey;//建筑概况

    @Lob
    public String orgAddress;//详细地址


    public static OrganizeGroup add(OrganizeGroupVO vo){
        OrganizeGroup org = new OrganizeGroup();
        org.name = vo.name;
        org.type = vo.type;
        org.buildTime = vo.buildTime;
        org.structure = vo.structure;
        org.region = Region.findById(vo.regionId);
        org.estate = vo.estate;
        org.area = vo.area;
        org.product = vo.product;
        org.scale = vo.scale;
        org.personNum = vo.personNum;
        if(StringUtils.isNotBlank(vo.site) &&  !vo.site.contains("http:")){
            vo.site = "http://"+vo.site;
        }
        org.site = vo.site;
        org.intro = vo.intro;
        org.licence = vo.licence;
        org.permit = vo.permit;
        org.districtUrl = vo.districtUrl;
        org.survey = vo.survey;
        org.orgAddress = vo.orgAddress;
        return org.save();
    }

    public void edit(OrganizeGroupVO vo){
        this.name = vo.name;
        this.type = vo.type;
        this.buildTime = vo.buildTime;
        this.structure = vo.structure;
        this.region = Region.findById(vo.regionId);
        this.estate = vo.estate;
        this.area = vo.area;
        this.product = vo.product;
        this.scale = vo.scale;
        this.personNum = vo.personNum;
        this.site = vo.site;
        this.intro = vo.intro;
        this.licence = vo.licence;
        this.permit = vo.permit;
        this.districtUrl = vo.districtUrl;
        this.survey = vo.survey;
        this.orgAddress = vo.orgAddress;
        this.save();
    }


    public static OrganizeGroup addPlat(){
        OrganizeGroup org = new OrganizeGroup();
        org.name = "巡查平台";
        org.isPlat = true;
        return org.save();
    }


    public static OrganizeGroup findPlat(){
        return OrganizeGroup.find(getDefaultContitionSql(" isPlat = 1 ")).first();
    }

    public static OrganizeGroup findByName(String name){
        return OrganizeGroup.find(getDefaultContitionSql(" name = ?"),name).first();
    }


    public static List<OrganizeGroup> fetchAll(){
        return find("select g from OrganizeGroup g where g.isDeleted = 0 ").fetch();
    }

    public static List<OrganizeGroup> fetchAllOrg(){
        return find("select g from OrganizeGroup g where g.isDeleted = 0 and g.isPlat = 0").fetch();
    }


    public static List<OrganizeGroup> fetchByRegions(List<Region> regions){
        if (regions.isEmpty()){
            return Collections.emptyList();
        }
        return find(getDefaultContitionSql(" region in (:regions)")).bind("regions",regions).fetch();

    }


}
