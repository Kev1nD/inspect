package utils;

import models.Person;
import models.PersonRole;
import models.Resource;
import models.RoleResource;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import play.mvc.Http;
import play.mvc.Scope;
import play.templates.JavaExtensions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by daikai on 2017/6/28.
 */
public class CommonUtils {

    private static final String LOGGED_PERSON_ID = "loggedPersonId";

    private static final String KEEP_LOGIN_ID = "KEEP_LOGIN_ID";

    private static final String FROM_PLAT_PERSONID ="FROM_PLAT_PERSONID";


    public static String formatDate(Date date, String format) {
        return JavaExtensions.format(date, format);
    }

    public static String formatDate(Long date, String format) {
        return JavaExtensions.format(new Date(date), format);
    }

    public static String getDataFromCookie(String arg) {
        Map<String, Http.Cookie> cookies = play.mvc.Http.Request.current().cookies;
        Http.Cookie cookie = cookies.get(arg);
        if (null != cookie) {
            return cookie.value;
        }
        return null;
    }

    public static void savePersonToSession(Long personId) {
        if (personId != null) {
            Scope.Session.current().put(LOGGED_PERSON_ID, personId);
        } else {
            Scope.Session.current().remove(LOGGED_PERSON_ID);
        }
    }

    public static String currentPersonId() {
        String personId = Scope.Session.current().get(LOGGED_PERSON_ID);
        if (personId == null) {
            personId = getDataFromCookie(KEEP_LOGIN_ID);
        }
        return personId;
    }

    public static Person currPerson(){
        String personId = currentPersonId();
        if (StringUtils.isNotBlank(personId)) {
            return Person.findById(Long.parseLong(personId));
        } else {
            return null;
        }
    }


    public static Integer compareDate(String dateStr1,String dateStr2){
        if (StringUtils.isBlank(dateStr1)){
            return  null;
        }
        if (StringUtils.isBlank(dateStr2)){
            return  null;
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") ;
        try {
            Date date1 = dateFormat.parse(dateStr1);
            Date date2 = dateFormat.parse(dateStr2);
            if(date1.getTime() > date2.getTime()){
                return  1;
            }else if(date1.getTime() < date2.getTime()){
                return -1;
            }else{
                return  0;
            }
        } catch (Exception e) {
            return  null;
        }

    }



    public static Integer diffDate(String dateStr1,String dateStr2){
        if (StringUtils.isBlank(dateStr1)){
            return  null;
        }
        if (StringUtils.isBlank(dateStr2)){
            return  null;
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") ;
        try {
            Date date1 = dateFormat.parse(dateStr1);
            Date date2 = dateFormat.parse(dateStr2);
            long diffTime = date1.getTime() - date2.getTime();
            return (int)(diffTime/(24 * 60 * 60 * 1000));
        } catch (Exception e) {
            return  null;
        }

    }

    /**
     * @Description:起始时间，增加天数
     * @Date: 下午1:47 2017/9/30
     */
    public static String addDate(String dateStr1,String dateStr2){
        if (StringUtils.isBlank(dateStr1)){
            return  null;
        }
        if (StringUtils.isBlank(dateStr2)){
            return  null;
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") ;
        try {
            Date date = dateFormat.parse(dateStr1);
            Calendar calendar  = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE,Integer.valueOf(dateStr2));
            return dateFormat.format(calendar.getTime());
        } catch (Exception e) {
            return  null;
        }

    }



    public static String lastdate(String dateStr1){
        if (StringUtils.isBlank(dateStr1)){
            return  null;
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") ;
        try {
            Date date = dateFormat.parse(dateStr1);
            Calendar calendar  = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
            return dateFormat.format(calendar.getTime());
        } catch (Exception e) {
            return  null;
        }

    }



    public static void savePlatSession(Long personId){
        if (personId != null) {
            Scope.Session.current().put(FROM_PLAT_PERSONID, personId);
        } else {
            Scope.Session.current().remove(FROM_PLAT_PERSONID);
        }
    }

    public static Long getFromPlatPersonId(){
        String personIdStr = Scope.Session.current().get(FROM_PLAT_PERSONID);
        if(StringUtils.isNotBlank(personIdStr)){
            return Long.valueOf(personIdStr);
        }else{
            return  null;
        }

    }








}
