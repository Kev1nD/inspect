package utils;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import play.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by daikai on 2017/8/3.
 */
public class PictureUtils {



    //图片压缩
    public static void reduceImg(File file,Integer width,Integer height,Float rate){
        try {
            System.err.println("压缩前图片大小："+file.length());
            Image src = ImageIO.read(file);
            // 如果rate不为空说明是按比例压缩
            if (rate != null && rate > 0) {
                // 获取文件高度和宽度
                int[] results = getImgWidth(file);
                if (results == null || results[0] == 0 || results[1] == 0) {
                    return;
                } else {
                    width = (int) (results[0] * rate);
                    height = (int) (results[1] * rate);
                }
            }
            BufferedImage tag = new BufferedImage(width,height,BufferedImage.TYPE_INT_BGR);
            tag.getGraphics().drawImage(src.getScaledInstance(width,height,Image.SCALE_SMOOTH),0,0,null);

            FileOutputStream out = new FileOutputStream(file);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(tag);
            out.close();


            System.err.println("压缩后大小："+file.length());
        }catch (Exception e){
            Logger.error(e.getMessage());
        }finally {

        }
    }

    public static int[] getImgWidth(File file) {
        InputStream is = null;
        BufferedImage src = null;
        int result[] = { 0, 0 };
        try {
            is = new FileInputStream(file);
            src = javax.imageio.ImageIO.read(is);
            result[0] = src.getWidth(null); // 得到源图宽
            result[1] = src.getHeight(null); // 得到源图高
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}
