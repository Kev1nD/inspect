package utils;

import org.apache.commons.io.FileUtils;
import play.Logger;
import play.Play;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 文件上传工具类
 * Created by daikai on 2017/7/1.
 */
public class FileUploadUtils {


    private static final  String FILE_BASE_URL = "/documentation/";

    public static File createTmpFile(String fileName){
        File parentFile = new File("/documentation/");
        if (!parentFile.exists() || !parentFile.isDirectory()){
            parentFile.mkdirs();
        }
        return  new File(FILE_BASE_URL+System.currentTimeMillis()+fileName);
    }



    public static File getFileFromInput(String fileName) throws IOException {
        play.mvc.Http.Request request = play.mvc.Http.Request.current();
        File file = createTmpFile(fileName);
        InputStream is = null;
        try {
            if (request.isNew) {
                is = request.body;
                FileUtils.copyInputStreamToFile(is, file);
                if (file == null || !file.exists() || file.length() == 0l
                        || file.length() > getUploadLimit() * 1024 * 1024) {
                    return null;
                }
                return file;
            }
        } catch (IOException e) {
            Logger.error(e, "file upload is fail", fileName);
            throw e;
        } finally {
            if (is != null) {
                is.close();
            }
            FileUtils.deleteQuietly(file);
        }
        return null;
    }

    public static int getUploadLimit() {
        return Integer.parseInt(Play.configuration.getProperty(
                "file.UploadLimit", "100"));
    }





}
