package utils;

import play.Logger;
import play.Play;
import play.mvc.Before;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by daikai on 2017/9/22.
 */


public class BatchSqlUtils {

    private static final String username = Play.configuration.getProperty("db.user");
    private static final String password = Play.configuration.getProperty("db.pass");
    private static final String driver = Play.configuration.getProperty("db.driver");
    private static String connectStr = Play.configuration.getProperty("db.url");


    /**
     * 批量插入
     * @param insertSql
     * @param params
     * @throws Exception
     */
    public synchronized  static void batchInsert(String insertSql, List<List<String>> params) throws Exception{
        long time1 = System.currentTimeMillis();
        Class.forName(driver);
        connectStr += "&useServerPrepStmts=false&rewriteBatchedStatements=true";// 此处是测试高效批次插入，去掉之后执行时普通批次插入
        Connection conn = DriverManager.getConnection(connectStr, username, password);
        conn.setAutoCommit(false); // 设置手动提交
        PreparedStatement psts = conn.prepareStatement(insertSql);
        params.stream().forEach(param -> {
            try {
                for (int i = 1; i <= param.size(); i++) {
                    psts.setObject(i, param.get(i - 1));
                }
                psts.addBatch(); // 加入批量处理
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        psts.executeBatch(); // 执行批量处理
        conn.commit(); // 提交
        conn.close();
        long time2 = System.currentTimeMillis();
        Logger.info("批处理执行结束，执行数量:"+params.size()+"，耗时:"+(time2-time1)+"ms");
    }











}
