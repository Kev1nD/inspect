package jobs;

import models.*;
import org.hibernate.Session;
import play.db.jpa.JPA;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import vos.PersonVO;

import java.util.List;

/**
 * Created by daikai on 2017/7/2.
 */

@OnApplicationStart
public class StartUp extends Job{


    @Override
    public void doJob() throws Exception {
        super.doJob();
        //initResource();
        //updateColumn();
    }



    public static void updateColumn() {
        Session session = (Session) JPA.em().getDelegate();
        if (!session.getTransaction().isActive()) {
            session.getTransaction().begin();
        }
        session.createSQLQuery("update Person set isLeave = 0 where isLeave is null ").executeUpdate();
        session.createSQLQuery("update Goods set isNeedRepair = 0 where isNeedRepair is null ").executeUpdate();
        session.getTransaction().commit();

    }



    public static void initResource(){

        Resource resource = Resource.findbycode("s2");
        if (resource == null){
            Resource.add("单位巡查对象数量列表","s1");
            Resource.add("单位巡查对象“S”编号列表","s2");
            Resource.add("单位建筑及分布图情况列表","s3");
            Resource.add("单位巡查对象不合格情况列表","s4");
            Resource.add("当月单位不合格数状况列表","s5");
            Resource.add("单位巡查对象不在使用中的数量列表","s6");
            Resource.add("单位巡查对象的生产、使用、检修、报废日期列表","s7");
            Resource.add("单位相关人员列表","s8");
            Resource.add("单位产品、规模列表","s9");
            Resource.add("单位供应商列表","s10");
        }
    }


}
