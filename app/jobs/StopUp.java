package jobs;

import play.Logger;
import play.jobs.Job;
import play.jobs.OnApplicationStop;

@OnApplicationStop
public class StopUp extends Job{

    @Override
    public void doJob() throws Exception {
        super.doJob();
        System.err.println("=============》"+2);
        Logger.error("play 服务停止");
    }
}
