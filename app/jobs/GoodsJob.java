package jobs;

import models.GoodsLog;
import models.OrganizeGroupGoods;
import models.enums.GoodStatus;
import models.enums.InspectStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang.time.DurationFormatUtils;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import play.jobs.OnApplicationStart;
import utils.CommonUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 记录跑批
 * Created by daikai on 2017/7/13.
 */

@On("0 0 0 ? * *")
public class GoodsJob extends Job{

    @Override
    public void doJob() throws Exception {
        super.doJob();
        batchImportGoods();
        //待巡检超时变成已过期数据
        checkExpire();
        //巡检对象需要维修
        needRepairJob();
    }


    public static void  batchImportGoods() throws Exception{
            Date today = new Date();
            DateFormat df = new SimpleDateFormat("dd");
            //今天日数
            String dayStr = df.format(today);

            //获取当月最后一天
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DATE,calendar.getActualMaximum(Calendar.DATE));
            String lastday = df.format(calendar.getTime());


            Logger.info("*********定时job开始 今天是"+dayStr+"号***********");
            List<OrganizeGroupGoods>  orgGoods = OrganizeGroupGoods.fetchForCheck();
            orgGoods.stream().forEach(og ->{
                if (og.goods.cycleTime.equals("0.5")){
                    //月中
                    int beginDay = 15 - Integer.valueOf(og.goods.validTime);
                    String beginDayStr = String.valueOf(beginDay);
                    if(beginDay < 10){
                        beginDayStr = "0"+beginDayStr;
                    }
                    if(StringUtils.equals(dayStr,beginDayStr)){
                        GoodsLog.add(og.goods,og.group);
                    }
                    //月末
                    int beginDay2 = Integer.valueOf(lastday) - Integer.valueOf(og.goods.validTime) *2;
                    String beginDay2Str = String.valueOf(beginDay2);
                    if(beginDay2 < 10){
                        beginDay2Str = "0"+beginDay2Str;
                    }
                    if(StringUtils.equals(dayStr,beginDay2Str)){
                        GoodsLog.add(og.goods,og.group);
                    }

                }else{

                  if(compareYearMonth(og.goods.usedate,og.goods.cycleTime)){
                      int beginDay = Integer.valueOf(lastday) - Integer.valueOf(og.goods.validTime) *2;
                      String beginDayStr = String.valueOf(beginDay);
                      if(beginDay < 10){
                          beginDayStr = "0"+beginDayStr;
                      }
                      if(StringUtils.equals(dayStr,beginDayStr)){
                          GoodsLog.add(og.goods,og.group);
                      }
                  }

                }
            });

            Logger.info("*********定时job结束***********");
    }


    public static Boolean compareYearMonth(String usedate,String cycleTime){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,Integer.valueOf(StringUtils.substringBefore(usedate,"-")));
        calendar.set(Calendar.MONTH,Integer.valueOf(StringUtils.substringBetween(usedate,"-","-")));
        calendar.add(Calendar.MONTH,Integer.valueOf(cycleTime)-1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        String checkTime = sdf.format(calendar.getTime());

        long time = System.currentTimeMillis();
        String nowMonth = sdf.format(time);
        if(StringUtils.equals(checkTime,nowMonth)){
            return true;
        }else{
            return  false;
        }
    }




    //跑批已过期的数据
    public static void checkExpire(){
        List<GoodsLog> goodsLogs =  GoodsLog.findByStatus(InspectStatus.待巡检);
        goodsLogs.stream().forEach(gl -> {
            //巡检开始生效时间
            Long nowTime = System.currentTimeMillis();
            if((nowTime - gl.createTime)> Long.valueOf(gl.goods.validTime)*24*60*3600){
                gl.editIStatus(InspectStatus.未巡检);
            }
        });
    }



    //跑批待维修的巡检对象数据
    public static void needRepairJob(){
        Date today = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //今天
        String dayStr = df.format(today);

        List<OrganizeGroupGoods> organizeGroupGoods = OrganizeGroupGoods.fetchForRepair();
        organizeGroupGoods.stream().forEach(ogg -> {
//            if(ogg.goods.lastRepairedTime == null){//第一次修
//                if(CommonUtils.diffDate(dayStr,ogg.goods.usedate) >= Integer.valueOf(ogg.goods.repairPeriod)){
//                    ogg.goods.setIsNeedRepair(true);
//                }
//
//            }else{//不是第一次修
//
//                if(CommonUtils.diffDate(dayStr,CommonUtils.formatDate(ogg.goods.lastRepairedTime,"yyyy-MM-dd"))
//                        >= Integer.valueOf(ogg.goods.repairPeriod)){
//                    ogg.goods.setIsNeedRepair(true);
//                }
//
//            }

            //设定维修时间
            if(CommonUtils.compareDate(ogg.goods.repairPeriod,dayStr) > -1){
                ogg.goods.setIsNeedRepair(true);
            }




        });
    }



}
