package vos;

import models.Goods;
import models.GoodsLog;
import utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/9/28.
 */
public class AbnormalGoodsVO extends DataVO {


    public String areaName;

    public String goodsNum;


    public String goodsName;


    public String checkTime;


    public String shouldTime;

    public String shouldEndTime;

    public String checkStatus;


    public String checker;

    public String fzPer;


    public String goodsStatus;


    public String repairDate;


    public String scrapDate;


    public String imgUrl;





    public static List<AbnormalGoodsVO> fromLog(List<GoodsLog> goodsLogs){
        if(goodsLogs.isEmpty()){
            return Collections.emptyList();
        }
        List<AbnormalGoodsVO> abnormalGoodsVOS = new ArrayList<>();

        goodsLogs.stream().forEach(gl -> {
            AbnormalGoodsVO abnormalGoodsVO = new AbnormalGoodsVO();
            abnormalGoodsVO.id = gl.goods.id;
            abnormalGoodsVO.areaName = gl.goods.area.name;
            abnormalGoodsVO.goodsNum = gl.goods.number;
            abnormalGoodsVO.goodsName = gl.goods.name;
            abnormalGoodsVO.checkTime = gl.checkTime == null?"": CommonUtils.formatDate(gl.checkTime,"yyyy-MM-dd");
            abnormalGoodsVO.shouldTime = CommonUtils.formatDate(gl.createTime,"yyyy-MM-dd");
            if(gl.goods.cycleTime.equals("0.5")){
                abnormalGoodsVO.shouldEndTime =  CommonUtils.addDate(abnormalGoodsVO.shouldTime,Integer.valueOf(gl.goods.validTime)*2+"");
            }else{
                abnormalGoodsVO.shouldEndTime = CommonUtils.lastdate(abnormalGoodsVO.shouldTime);
            }
            abnormalGoodsVO.checkStatus = gl.status.toString();
            abnormalGoodsVO.checker = gl.goods.area.checker.name;
            abnormalGoodsVO.fzPer = gl.goods.area.checker.fzPerson.name;
            abnormalGoodsVO.goodsStatus = gl.goods.status.toString();
            abnormalGoodsVO.repairDate = gl.goods.repairPeriod;
            abnormalGoodsVO.scrapDate = gl.goods.scrapdate;
            abnormalGoodsVO.imgUrl = gl.imgUrls;
            abnormalGoodsVOS.add(abnormalGoodsVO);
        });
        return abnormalGoodsVOS;
    }




    public static List<AbnormalGoodsVO> fromGoods(List<Goods> goodsList){
        if(goodsList.isEmpty()){
            return Collections.emptyList();
        }
        List<AbnormalGoodsVO> abnormalGoodsVOS = new ArrayList<>();

        goodsList.stream().forEach(gl -> {
            AbnormalGoodsVO abnormalGoodsVO = new AbnormalGoodsVO();
            abnormalGoodsVO.id = gl.id;
            abnormalGoodsVO.areaName = gl.area.name;
            abnormalGoodsVO.goodsNum = gl.number;
            abnormalGoodsVO.goodsName = gl.name;
            abnormalGoodsVO.checkTime = "";
            abnormalGoodsVO.shouldTime = "";
            abnormalGoodsVO.shouldEndTime =  "";
            abnormalGoodsVO.checkStatus = "";
            abnormalGoodsVO.checker = gl.area.checker.name;
            abnormalGoodsVO.fzPer = gl.area.checker.fzPerson.name;
            abnormalGoodsVO.goodsStatus = gl.status.toString();
            abnormalGoodsVO.repairDate = gl.repairPeriod;
            abnormalGoodsVO.scrapDate = gl.scrapdate;
            abnormalGoodsVO.imgUrl = "";
            abnormalGoodsVOS.add(abnormalGoodsVO);
        });
        return abnormalGoodsVOS;
    }

}
