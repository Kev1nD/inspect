package vos;

import models.GoodsLog;
import models.OrganizeGroup;
import models.OrganizeGroupGoods;
import models.enums.GoodStatus;
import models.enums.InspectStatus;
import utils.CommonUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/9/4.
 */
public class StaFiveVO extends StaOneVO{

    public String yearMonth;



    public static List<StaFiveVO> toVOList(OrganizeGroup group){
        if(group == null){
            return Collections.EMPTY_LIST;
        }
        List<StaFiveVO> staFiveVOS  = new ArrayList<>();
        //当前月份
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int total1=0 , total2=0;
        Long total3 = 0l,total4 = 0l,total5 = 0l,total6 = 0l,total7 = 0l;
        for(int i =0;i<12;i++){
            calendar.set(Calendar.YEAR,currentYear);
            calendar.set(Calendar.MONTH,i);
            calendar.set(Calendar.HOUR_OF_DAY,calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE,calendar.getActualMinimum(Calendar.MINUTE));
            calendar.set(Calendar.SECOND,calendar.getActualMinimum(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND,calendar.getActualMinimum(Calendar.MILLISECOND));
            calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            Long startTime = calendar.getTimeInMillis();
            calendar.set(Calendar.HOUR_OF_DAY,calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE,calendar.getActualMaximum(Calendar.MINUTE));
            calendar.set(Calendar.SECOND,calendar.getActualMaximum(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND,calendar.getActualMaximum(Calendar.MILLISECOND));
            calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            Long endTime =  calendar.getTimeInMillis();


            StaFiveVO staFiveVO = new StaFiveVO();
            staFiveVO.yearMonth = CommonUtils.formatDate(endTime,"yyyy年MM月");
            staFiveVO.goodsNum = OrganizeGroupGoods.countGoodsByGroupAndTime(group,startTime,endTime).intValue();
            total1 += staFiveVO.goodsNum;
            staFiveVO.goodsFactNum = staFiveVO.goodsNum;
            total2 += staFiveVO.goodsFactNum;
            staFiveVO.expireNum  = GoodsLog.countGoodsByGroupAndStatus(group, InspectStatus.未巡检,startTime,endTime);
            total3 += staFiveVO.expireNum;
            Long fzcNum = OrganizeGroupGoods.countGoodsByGroupAndStatusAndTime(group, GoodStatus.非正常,startTime,endTime);
            staFiveVO.abNormalNum = fzcNum + OrganizeGroupGoods.countGoodsByGroupAndStatusAndTime(group, GoodStatus.维修中,startTime,endTime);
            total4 += staFiveVO.abNormalNum;
            staFiveVO.abNormalUsingNum = fzcNum;
            total5 += staFiveVO.abNormalUsingNum;

            staFiveVO.noRepairNum = OrganizeGroupGoods.countNoRepairGoodsByGroup(group);
            total6 += staFiveVO.noRepairNum;

            //已过报废时间
            Long havePassNum = OrganizeGroupGoods.countScrapGoodsByGroupAndTime(group,CommonUtils.formatDate(startTime,"yyyy-MM-dd"),
                    CommonUtils.formatDate(endTime,"yyyy-MM-dd"));

            staFiveVO.noScrapNum =havePassNum;
            total7 +=  staFiveVO.noScrapNum;
            staFiveVOS.add(staFiveVO);
        }
        StaFiveVO staFiveVO = new StaFiveVO();
        staFiveVO.yearMonth = "合计";
        staFiveVO.goodsNum = total1;
        staFiveVO.goodsFactNum = total2;
        staFiveVO.expireNum = total3;
        staFiveVO.abNormalNum = total4;
        staFiveVO.abNormalUsingNum = total5;
        staFiveVO.noRepairNum = total6;
        staFiveVO.noScrapNum = total7;
        staFiveVOS.add(staFiveVO);
        return staFiveVOS;
    }






    public static List<StaFiveVO> toVOListByGroups(List<OrganizeGroup> groups){
        if(groups == null){
            return Collections.EMPTY_LIST;
        }
        List<StaFiveVO> staFiveVOS  = new ArrayList<>();
        //当前月份
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int total1=0 , total2=0;
        Long total3 = 0l,total4 = 0l,total5 = 0l,total6 = 0l,total7 = 0l;
        for(int i =0;i<12;i++){
            calendar.set(Calendar.YEAR,currentYear);
            calendar.set(Calendar.MONTH,i);
            calendar.set(Calendar.HOUR_OF_DAY,calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE,calendar.getActualMinimum(Calendar.MINUTE));
            calendar.set(Calendar.SECOND,calendar.getActualMinimum(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND,calendar.getActualMinimum(Calendar.MILLISECOND));
            calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            Long startTime = calendar.getTimeInMillis();
            calendar.set(Calendar.HOUR_OF_DAY,calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE,calendar.getActualMaximum(Calendar.MINUTE));
            calendar.set(Calendar.SECOND,calendar.getActualMaximum(Calendar.SECOND));
            calendar.set(Calendar.MILLISECOND,calendar.getActualMaximum(Calendar.MILLISECOND));
            calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            Long endTime =  calendar.getTimeInMillis();


            StaFiveVO staFiveVO = new StaFiveVO();
            staFiveVO.yearMonth = CommonUtils.formatDate(endTime,"yyyy年MM月");
            staFiveVO.goodsNum = OrganizeGroupGoods.countGoodsByGroupsAndTime(groups,startTime,endTime).intValue();
            total1 += staFiveVO.goodsNum;
            staFiveVO.goodsFactNum = staFiveVO.goodsNum;
            total2 += staFiveVO.goodsFactNum;
            staFiveVO.expireNum  = GoodsLog.countGoodsByGroupsAndTimeStatus(groups, InspectStatus.未巡检,startTime,endTime);
            total3 += staFiveVO.expireNum;
            Long fzcNum = OrganizeGroupGoods.countGoodsByGroupsAndStatusAndTime(groups, GoodStatus.非正常,startTime,endTime);
            staFiveVO.abNormalNum = fzcNum +OrganizeGroupGoods.countGoodsByGroupsAndStatusAndTime(groups, GoodStatus.维修中,startTime,endTime) ;
            total4 += staFiveVO.abNormalNum;
            staFiveVO.abNormalUsingNum = fzcNum;
            total5 += staFiveVO.abNormalUsingNum;

            staFiveVO.noRepairNum = OrganizeGroupGoods.countNoRepairGoodsByGroups(groups);
            total6 += staFiveVO.noRepairNum;

            //已过报废时间
            Long havePassNum = OrganizeGroupGoods.countScrapGoodsByGroupsAndTime(groups,CommonUtils.formatDate(startTime,"yyyy-MM-dd"),
                    CommonUtils.formatDate(endTime,"yyyy-MM-dd"));

            staFiveVO.noScrapNum =havePassNum;
            total7 +=  staFiveVO.noScrapNum;
            staFiveVOS.add(staFiveVO);
        }
        StaFiveVO staFiveVO = new StaFiveVO();
        staFiveVO.yearMonth = "合计";
        staFiveVO.goodsNum = total1;
        staFiveVO.goodsFactNum = total2;
        staFiveVO.expireNum = total3;
        staFiveVO.abNormalNum = total4;
        staFiveVO.abNormalUsingNum = total5;
        staFiveVO.noRepairNum = total6;
        staFiveVO.noScrapNum = total7;
        staFiveVOS.add(staFiveVO);
        return staFiveVOS;
    }



}
