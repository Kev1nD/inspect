package vos;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import play.Logger;

/**
 * 网络请求结果
 *
 * @author gcz
 *
 */
public class Result {
	/**
	 * 返回状态：“succ”表示成功；“fail”表示失败
	 */
	public String status;
	/**
	 * 返回状态码
	 */
	public int code;

	public String message;

	public Long systemTime;

	/**
	 * 数据
	 */
	public Data data;

	public static class StatusCode {
		public static final Object[] SUCCESS = { 20000, "请求成功" };
		public static final Object[] NOT_LOGIN_ERROR = { 30000, "未登录" };
		public static final Object[] REGIST_PHONE_ERROR = { 30001, "手机号码格式错误" };
		public static final Object[] LOGIN_USER_NOTEXIST = { 30002, "用户不存在" };
		public static final Object[] LOGIN_PASSWORD_ERROR = { 30003, "密码错误" };


		public static final Object[] NOT_VALID_QRCODE_URL = { 40001, "无效的二维码地址" };
		public static final Object[] NOT_USED_QRCODE_URL = { 40002, "二维码暂未启用" };
		public static final Object[] NOT_VALID_AREA = { 40003, "巡检地点不在未检列表" };

		public static final Object[] GOOD_HAD_INSPECT = { 40004, "对象已经巡检或未在本期巡检范围" };


	}

	public Result() {
		systemTime = System.currentTimeMillis();
	}

	public static final ObjectMapper mapper = new ObjectMapper();

	static {
		mapper.getSerializationConfig().setSerializationInclusion(Inclusion.NON_NULL);
	}

	public static String failed(Object[] codemessage) {
		Result result = new Result();
		result.status = "fail";
		result.code = (int) codemessage[0];
		result.message = (String) codemessage[1];
		try {
			return mapper.writeValueAsString(result);
		} catch (IOException e) {
			Logger.info("result failed : " + e.getMessage());
			return null;
		}
	}

	public static String failed(Object[] codemessage, Data data) {
		Result result = new Result();
		result.status = "fail";
		result.code = (int) codemessage[0];
		result.message = (String) codemessage[1];
		result.data = data;
		try {
			return mapper.writeValueAsString(result);
		} catch (IOException e) {
			Logger.info("result failed : " + e.getMessage());
			return null;
		}
	}

	public static String succeed() {
		Result result = new Result();
		result.status = "true";
		result.code = (int) StatusCode.SUCCESS[0];
		result.message = (String) StatusCode.SUCCESS[1];
		try {
			return mapper.writeValueAsString(result);
		} catch (IOException e) {
			Logger.info("result failed : " + e.getMessage());
			return null;
		}
	}

	public static String succeed(String message) {
		Result result = new Result();
		result.status = "succ";
		result.code = (int) StatusCode.SUCCESS[0];
		result.message = message;
		try {
			return mapper.writeValueAsString(result);
		} catch (IOException e) {
			Logger.info("result failed : " + e.getMessage());
			return null;
		}
	}

	public static String succeed(Data data) {
		Result result = new Result();
		result.status = "succ";
		result.code = (int) StatusCode.SUCCESS[0];
		result.message = (String) StatusCode.SUCCESS[1];
		result.data = data;
		try {
			return mapper.writeValueAsString(result);
		} catch (IOException e) {
			Logger.info("result failed : " + e.getMessage());
			return null;
		}
	}

	public static String succeed(Data data, String message) {
		Result result = new Result();
		result.status = "succ";
		result.code = (int) StatusCode.SUCCESS[0];
		result.message = message;
		result.data = data;
		try {
			return mapper.writeValueAsString(result);
		} catch (IOException e) {
			Logger.info("result failed : " + e.getMessage());
			return null;
		}
	}

}
