package vos;

import models.*;
import models.enums.GoodStatus;
import models.enums.InspectStatus;
import org.apache.commons.lang.StringUtils;
import utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by daikai on 2017/9/2.
 */
public class StaOneVO extends StaVO{


    public int goodsNum;

    public int goodsFactNum;


    public Long expireNum;//过期数量

    public Long abNormalNum;//非正常数量

    public Long abNormalUsingNum;//非正常还在使用数量

    public Long noRepairNum;//没有维修数量

    public Long noScrapNum;//没有报废数量


    public static List<StaOneVO> convertFromOrgList(List<OrganizeGroup> organizeGroups){
        if(organizeGroups.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        List<StaOneVO> staOneVOS = new ArrayList<>();
        organizeGroups.stream().forEach(org -> {
            StaOneVO staOneVO = new StaOneVO();
            List<Goods> goods = OrganizeGroupGoods.findByGroup(org);
            staOneVO.goodsNum = goods == null?0:goods.size();
            staOneVO.goodsFactNum = staOneVO.goodsNum;
            List<OrganizeGroup> groups = new ArrayList<>();
            groups.add(org);
            staOneVO.expireNum = GoodsLog.countGoodsByGroupsAndStatus(groups, InspectStatus.未巡检);
            Long fzcNum =  OrganizeGroupGoods.countGoodsByGroupAndStatus(org,GoodStatus.非正常);
            staOneVO.abNormalNum = fzcNum+
                    OrganizeGroupGoods.countGoodsByGroupAndStatus(org,GoodStatus.维修中);
            staOneVO.abNormalUsingNum = fzcNum;

            staOneVO.noRepairNum = OrganizeGroupGoods.countNoRepairGoodsByGroup(org);

            //已过报废时间
            Long havePassNum = OrganizeGroupGoods.countScrapGoodsByGroup(org,
                    CommonUtils.formatDate(System.currentTimeMillis(),"yyyy-MM-dd"));
            Long scrapNum = OrganizeGroupGoods.countGoodsByGroupAndStatus(org,GoodStatus.已报废);

            staOneVO.noScrapNum =havePassNum-scrapNum;
            staOneVO.orgName = org.name;
            staOneVO.id = org.id;
            staOneVO.province = org.region.parentArea.parentArea.parentArea.name;

            staOneVO.city=org.region.parentArea.parentArea.name;

            staOneVO.district=org.region.parentArea.name;

            staOneVO.street=org.region.name;

            staOneVOS.add(staOneVO);

        });
        return staOneVOS;

    }





}
