package vos;

import models.Notice;
import utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/9/3.
 */
public class NoticeVO extends DataVO{

    public String title;

    public String content;

    public String createTime;



    public NoticeVO(){

    }


    public NoticeVO(Notice notice){
        this.title = notice.title;
        this.content = notice.content;
        this.createTime = CommonUtils.formatDate(notice.createTime,"yyyy/MM/dd");
    }



    public static List<NoticeVO> toList(List<Notice> notices){
        if(notices.isEmpty()){
            return Collections.emptyList();
        }
        List<NoticeVO> noticeVOS = new ArrayList<>();
        notices.stream().forEach(n -> {
            NoticeVO noticeVO = new NoticeVO(n);
            noticeVOS.add(noticeVO);
        });
        return  noticeVOS;

    }

}
