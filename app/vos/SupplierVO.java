package vos;

import models.Supplier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/7/11.
 */
public class SupplierVO extends DataVO {

    public String type;

    public String name;

    public String address;

    public String contact;

    public String job;

    public String mobile;

    public Boolean isQualified;//是否有资质

    public String licence;


    public String permit;

    public String remark;

    public String registNum;

    public String regionId;


    public String province;

    public String city;

    public String district;

    public String street;


    public String manager;

    public String manageMobile;



    public static List<SupplierVO> convertFromSuppliers(List<Supplier> suppliers){
        if(suppliers.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        List<SupplierVO> supplierVOS = new ArrayList<>();
        suppliers.stream().forEach(s ->{
            SupplierVO supplierVO = new SupplierVO();
            supplierVO.type = s.type;
            supplierVO.name = s.name;
            supplierVO.contact = s.contact;
            supplierVO.job = s.job;
            supplierVO.mobile = s.mobile;
            supplierVO.isQualified = s.isQualified;
            supplierVO.licence  = s.licence;
            supplierVO.permit =s.permit;
            supplierVO.province = s.region ==null?"":s.region.parentArea.parentArea.parentArea.name;
            supplierVO.city = s.region ==null?"":s.region.parentArea.parentArea.name;
            supplierVO.district =s.region ==null?"":s.region.parentArea.name;
            supplierVO.street= s.region ==null?"":s.region.name;
            supplierVO.address =s.address;
            supplierVO.manager = s.manager;
            supplierVO.manageMobile = s.manageMobile;
            supplierVOS.add(supplierVO);
        });
        return supplierVOS;
    }



}
