package vos;

import models.FloorPics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by daikai on 2017/7/27.
 */
public class FloorPicsVO extends DataVO{

    public String name;


    public String imgUrl;


    public FloorPicsVO(String name,String imgUrl){
        this.name = name;
        this.imgUrl = imgUrl;
    }




    public static List<FloorPicsVO> convertToList(List<FloorPics> fps){
        if(fps.isEmpty()){
            return Collections.emptyList();
        }
        List<FloorPicsVO> floorPicsVOS = new ArrayList<>();
        fps.stream().forEach(fp -> {
            FloorPicsVO floorPicsVO = new FloorPicsVO(fp.name,fp.imgUrl);
            floorPicsVOS.add(floorPicsVO);
        });
        return  floorPicsVOS;

    }
}
