package vos;

import models.Abnormal;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by daikai on 2017/8/14.
 */
public class AbnormalVO extends DataVO{


    public String name;


    public AbnormalVO(){

    }


    public AbnormalVO(Abnormal abnormal){
        this.name = abnormal.name;
    }



    public static List<AbnormalVO> tolist(List<Abnormal> abnormals){
        if(abnormals.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        return  abnormals.stream().map(a -> new AbnormalVO(a)).collect(Collectors.toList());
    }

}
