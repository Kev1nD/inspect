package vos;

import models.*;
import models.enums.GoodStatus;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by daikai on 2017/9/2.
 */
public class StaSixVO extends StaVO{


    public String goodsNames;

    public String goodsTypes;

    public Long repairNum;

    public Long scrapNum;


    public static List<StaSixVO> convertFromOrgList(List<OrganizeGroup> organizeGroups){
        if(organizeGroups.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        List<StaSixVO> staSixVOS = new ArrayList<>();
        organizeGroups.stream().forEach(org -> {

            List<Goods> goods = OrganizeGroupGoods.findByGroupNameAndType(org);
            if(goods != null && goods.size() > 0) {
                for(Goods g:goods) {
                    StaSixVO staSixVO = new StaSixVO();
                    staSixVO.goodsNames = g.name;
                    staSixVO.goodsTypes = g.type;
                    staSixVO.repairNum = OrganizeGroupGoods.countGoodsByGroupAndStatus(org, GoodStatus.维修中,g);
                    staSixVO.scrapNum = OrganizeGroupGoods.countGoodsByGroupAndStatus(org, GoodStatus.已报废,g);

                    staSixVO.orgName = org.name;

                    staSixVO.id = org.id;
                    staSixVO.province = org.region.parentArea.parentArea.parentArea.name;
                    staSixVO.city = org.region.parentArea.parentArea.name;
                    staSixVO.district = org.region.parentArea.name;
                    staSixVO.street = org.region.name;

                    staSixVOS.add(staSixVO);
                }
            }

        });
        return staSixVOS;

    }





}
