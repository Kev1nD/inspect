package vos;


import java.util.List;

/**
 * Created by daikai on 2017/8/31.
 */
public class StaVO {

    public String orgName;

    public String checker;

    public String mobile;

    public String province;

    public String city;

    public String district;

    public String street;

    public Long id;


    public StaVO(){

    }


    public StaVO(String orgName,String checker,String mobile,
                    String province,String city,String district,String street){
        this.orgName = orgName;
        this.checker = checker;
        this.mobile = mobile;
        this.province = province;
        this.city = city;
        this.district = district;
        this.street = street;
    }
}
