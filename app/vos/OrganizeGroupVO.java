package vos;

import models.*;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by daikai on 2017/7/8.
 */
public class OrganizeGroupVO extends DataVO{


    public String name;

    public String type;//以逗号隔开

    public String buildTime;//建筑时间

    public String structure;//建筑结构

    public Long regionId;//地理位置

    public String region;

    public String estate;//物业

    public String area;//经营范围

    public String product;//生产产品

    public String scale;//生产规模

    public String personNum;//总人数

    public String site;//网址

    public String intro;//简介

    public String licence;//营业执照

    public String permit;//营业许可证

    public String districtUrl;//总分布图

    public String survey;//建筑概况

    public String picIds;//楼层分布图

    public List<FloorPicsVO> pics;//楼层分布图

    public String legalPer;//法人

    public String managePer;//经理

    public String manageMobile;//经理电话


    public boolean isPlat;

    public String curRegionLevel;
    public String curRegionName;
    public Long curRegionId;


    //统计专用
    public String fzPer;

    public String fzMobile;

    public String aqPer;

    public String aqMobile;

    public List<PersonVO> xjPers;

    public String province;

    public String city;

    public String district;

    public String street;

    public String orgAddress;//详细地址

    public String orgAdminName;




    public OrganizeGroupVO(){

    }


    public OrganizeGroupVO(OrganizeGroup group){
        this.isPlat = group.isPlat;
        this.name = group.name;
        if(!group.isPlat) {
            this.region = group.region.parentArea.parentArea.parentArea.name +
                    group.region.parentArea.parentArea.name +
                    group.region.parentArea.name +
                    group.region.name;
        }
        List<Person> personList = OrganizeGroupPerson.findPersonbyGroup(group);
        personList = personList.stream().filter(p -> StringUtils.isNotBlank(p.job)).collect(Collectors.toList());
        for(Person p:personList){
            if(p.job.equals("企业法人")){
                this.legalPer = p.name;
            }
            if(p.job.equals("总经理")){
                this.managePer = p.name;
                this.manageMobile = p.mobile;
            }
        }

        this.survey = group.survey;
        this.districtUrl = group.districtUrl;
        List<FloorPics> fps = OrganizeGroupPics.fetchByGroup(group);
        this.pics =FloorPicsVO.convertToList(fps);
    }





    //统计
    public static List<OrganizeGroupVO> toListByStatist(List<OrganizeGroup> groups){
        if (groups.isEmpty()){
            return Collections.emptyList();
        }
        List<OrganizeGroupVO> organizeGroupVOS = new ArrayList<>();
        groups.stream().forEach(g -> {
            OrganizeGroupVO vo = new OrganizeGroupVO();
            List<Person> personList = OrganizeGroupPerson.findPersonbyGroup(g);
            personList = personList.stream().filter(p -> StringUtils.isNotBlank(p.job)).collect(Collectors.toList());
            List<PersonVO> xjPersons = new ArrayList<>();
            for(Person p:personList){
                if(p.job.equals("总经理")){
                    vo.managePer = p.name;
                    vo.manageMobile = p.mobile;
                }

            }
            vo.id = g.id;
            vo.name = g.name;
            vo.province = g.region.parentArea.parentArea.parentArea.name;
            vo.city = g.region.parentArea.parentArea.name;
            vo.district = g.region.parentArea.name;
            vo.street = g.region.name;
            vo.type = g.type;
            vo.structure = g.structure;
            vo.buildTime =g.buildTime;
            vo.survey = g.survey;
            vo.districtUrl = g.districtUrl;


            organizeGroupVOS.add(vo);
        });
        return organizeGroupVOS;

    }


}
