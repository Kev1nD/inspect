package vos;

import models.*;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by daikai on 2017/9/2.
 */
public class StaFourVO extends StaVO{


    public String goodsName;

    public String goodsType;

    public String abNormalContent;


    public static List<StaFourVO> convertFromOggList(List<OrganizeGroupGoods> organizeGroupGoods){
        if(organizeGroupGoods.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        List<StaFourVO> staFourVOS = new ArrayList<>();
        organizeGroupGoods.stream().forEach(ogg -> {
            StaFourVO staFourVO = new StaFourVO();
            staFourVO.id = ogg.group.id;
            staFourVO.goodsName = ogg.goods.name;
            staFourVO.goodsType = ogg.goods.type;
            GoodsLog goodsLog =  GoodsLog.findByGoods(ogg.goods);
            staFourVO.abNormalContent = goodsLog == null?"":goodsLog.reason;

            staFourVO.orgName = ogg.group.name;

            List<Person> personList = OrganizeGroupPerson.findPersonbyGroup(ogg.group);
            personList = personList.stream().filter(p -> StringUtils.isNotBlank(p.job)).collect(Collectors.toList());
            for(Person p:personList){
                if(p.job.equals("负责人")){
                    staFourVO.checker  = p.name;
                    staFourVO.mobile  = p.mobile;
                }
            }


            staFourVO.province = ogg.group.region.parentArea.parentArea.parentArea.name;

            staFourVO.city=ogg.group.region.parentArea.parentArea.name;

            staFourVO.district=ogg.group.region.parentArea.name;

            staFourVO.street=ogg.group.region.name;

            staFourVOS.add(staFourVO);

        });
        return staFourVOS;

    }





}
