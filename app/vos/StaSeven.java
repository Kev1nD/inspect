package vos;

import models.OrganizeGroupGoods;
import models.RepairLog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/9/3.
 */
public class StaSeven extends StaVO {

    public String goodsName;

    public String goodsType;

    public String createdate;

    public String usedate;

    public String repairdate;

    public String scrapdate;

    public String producer;

    public String repairer;

    public String repairPeriod;


    public static List<StaSeven> toList(List<OrganizeGroupGoods> organizeGroupGoods){
        if(organizeGroupGoods.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        List<StaSeven> staSevens = new ArrayList<>();
        organizeGroupGoods.stream().forEach(ogg -> {
            StaSeven staSeven = new StaSeven();
            staSeven.id = ogg.group.id;
            staSeven.goodsName = ogg.goods.name;
            staSeven.goodsType = ogg.goods.type;
            staSeven.createdate = ogg.goods.createdate;
            staSeven.usedate = ogg.goods.usedate;
            staSeven.repairdate = ogg.goods.repairdate;
            staSeven.repairPeriod = ogg.goods.repairPeriod;
            staSeven.scrapdate = ogg.goods.scrapdate;
            staSeven.producer = ogg.goods.producer == null?"":ogg.goods.producer.name;
            staSeven.orgName = ogg.group.name;
            staSeven.province = ogg.group.region.parentArea.parentArea.parentArea.name;
            staSeven.city = ogg.group.region.parentArea.parentArea.name;
            staSeven.district = ogg.group.region.parentArea.name;
            staSeven.street = ogg.group.region.name;
            //
            RepairLog log = RepairLog.findByGoods(ogg.goods);
            staSeven.repairer = log == null?"":(log.supplier == null?"":log.supplier.name);
            staSevens.add(staSeven);
        });
        return  staSevens;
    }





}
