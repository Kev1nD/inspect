package vos;

import models.Goods;
import models.GoodsLog;
import models.OrganizeGroup;
import models.OrganizeGroupGoods;
import models.enums.GoodStatus;
import models.enums.InspectStatus;
import utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/9/2.
 */
public class StaElevenVO extends StaVO{


    public String goodsName;

    public String goodsType;

    public Long goodsNum;

    public Long goodsFactNum;



    public static List<StaElevenVO> toList(List<OrganizeGroup> groups){
        List<StaElevenVO> list = new ArrayList<>();
        if(groups.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        groups.stream().forEach(org ->{
            List<Goods> goods = OrganizeGroupGoods.findByGroupNameAndType(org);
            if(goods != null && goods.size() > 0) {
                for(Goods g:goods) {
                    StaElevenVO staElevenVO = new StaElevenVO();
                    staElevenVO.goodsName = g.name;
                    staElevenVO.goodsType = g.type;
                    staElevenVO.goodsFactNum = OrganizeGroupGoods.countByGroupNameAndType(org,g.name,g.type);
                    //暂时
                    staElevenVO.goodsNum = staElevenVO.goodsFactNum;
                    staElevenVO.orgName = org.name;

                    staElevenVO.id = org.id;
                    staElevenVO.province = org.region.parentArea.parentArea.parentArea.name;
                    staElevenVO.city = org.region.parentArea.parentArea.name;
                    staElevenVO.district = org.region.parentArea.name;
                    staElevenVO.street = org.region.name;

                    list.add(staElevenVO);
                }
            }

        });

        return list;
    }




}
