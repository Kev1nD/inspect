package vos;

import models.*;
import org.apache.commons.lang.StringUtils;
import utils.CommonUtils;

import javax.validation.constraints.Null;
import java.util.List;

/**
 * Created by daikai on 2017/6/28.
 */
public class PersonVO extends  DataVO{

    public String username;

    public String name;

    public String password;

    public String sex;

    public String avator;//头像

    public String job;

    public String mobile;

    public String contact;

    public String officedate;//任职时间

    public String leavedate;//离职时间


    public String description;//岗位描述

    public String remark;

    public String company;

    public Long fuzePersonId;//负责人ID

    public String zName;//总经理

    public String zmobile;

    public String fName;//负责人


    public String fmobile;


    public List<AreaVO> areas;//负责巡查点

    public Boolean isPlatPer = false;//是否是平台人员

    public Boolean isOrgAdmin = false;//是否是企业管理员

    public String recentCheckTime;//巡检员上次巡检时间

    public String nextCheckTime;//巡检员下次巡检时间

    public PersonVO(){

    }


    public PersonVO(Person person){
        this.name  = person.name;
        this.contact = person.contact;
        this.sex = person.sex;
        this.mobile = person.mobile;
        this.officedate = person.officedate;
        this.company = person.company;
        OrganizeGroupPerson ogp   = OrganizeGroupPerson.findByPerson(person);
        List<Person> persons  = OrganizeGroupPerson.findPersonbyGroup(ogp.group);
        for(Person person1:persons){
            if(StringUtils.isNotBlank(person1.job) && person1.job.equals("总经理")){
                this.zName = person1.name;
                this.zmobile = person1.mobile;
            }
        }
        this.fName = person.fzPerson == null?"":person.fzPerson.name;
        this.fmobile = person.fzPerson== null?"":person.fzPerson.mobile;
        List<Area> areas = Area.fetchByChecker(person);
        this.areas = AreaVO.convertTolist(areas);
        this.isPlatPer = ogp.group.isPlat;
        this.job = person.job;
        PersonRole personRole = PersonRole.findByPerson(person);
        if(personRole != null && personRole.role != null)
        this.isOrgAdmin = personRole.role.roleName.equals("企业管理员");
        if(StringUtils.equals(person.job,"巡检员")){
            GoodsLog log = GoodsLog.findCheckedLogByPerson(person);
            if(log != null)
            this.recentCheckTime  = CommonUtils.formatDate(log.checkTime,"yyyy/MM/dd");
        }


    }


//    public PersonVO(String name,String password,String sex,String avator,String job,String mobile,String contact,String officedate,
//                     String description,String remark,Long fuzePersonId){
//
//        this.name = name;
//        this.password = password;
//        this.sex = sex;
//        this.avator =avator;
//        this.job = job;
//        this.mobile = mobile;
//        this.contact = contact;
//        this.officedate = officedate;
//        this.leavedate = leavedate;
//        this.description = description;
//        this.fuzePersonId = fuzePersonId;
//        this.remark = remark;
//
//    }
}
