package vos;

import models.Area;
import models.Goods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by daikai on 2017/7/3.
 */
public class AreaVO  extends DataVO{

    public String number;

    public String name;

    public String address;

    public String imageUrl;

    public Long checkerId;//检查人

    public String remark;

    public List<GoodsVO> goods;

    public Integer goodsSize;

    public Boolean isValid = true;//是否需要验证 app端跳转需要

    public AreaVO(){

    }


    public AreaVO(Area area){
        this.id = area.id;
        this.number = area.number;
        this.name = area.name;
        this.address = area.address;
        this.imageUrl = area.imageUrl;
        this.checkerId = area.checker.id;
        this.remark = area.remark;
        this.goods = GoodsVO.convertFromGoodsLog(Goods.findByArea(area));
        this.goodsSize = this.goods == null?0:this.goods.size();
    }

    public static  List<AreaVO> convertTolist(List<Area> areas){
       if(areas.isEmpty()){
           return Collections.emptyList();

       }
       List<AreaVO> areaVOS = new ArrayList<>();
       areas.stream().forEach(a -> {
           AreaVO areaVO = new AreaVO(a);
           areaVOS.add(areaVO);
       });
       return  areaVOS;
    }





}
