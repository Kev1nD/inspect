package vos;

import models.Goods;
import models.GoodsLog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daikai on 2017/7/3.
 */
public class GoodsVO  extends DataVO{


    public  String number;

    public String name;

    public String type;

    public String snumber;//检查对象S编号

    public String status;//检查状态

    public String createdate;//生产日期

    public String usedate;//使用日期

    public String repairdate;//维修日期

    public String scrapdate;//报废日期

    public String imgUrl;

    public Long areaId;

    public String areaName;

    public String areaAddress;

    public Long producerId;//生产单位

    public Long supplyerId;//生产单位

    public String abnormalIds;



    public String cycleTime;//巡检周期（半个月，1个月）


    public String validTime;//巡检有效期（以天数为单位）

    public String inspectStatus;//对应巡检记录中的巡检状态

    public String shStatus;

    public String repairPeriod;

    public GoodsVO(){

    }




    public GoodsVO (Goods goods){
        this.id = goods.id;
        this.areaId = goods.area.id;
        this.number = goods.number;
        this.name = goods.name;
        this.type = goods.type;
        this.snumber = goods.snumber;
        this.status = goods.status.toString();
        this.createdate= goods.createdate;
        this.usedate = goods.usedate;
        this.repairdate = goods.repairdate;
        this.scrapdate = goods.scrapdate;
        this.imgUrl = goods.imgUrl;
        this.cycleTime = goods.cycleTime;
        this.validTime = goods.validTime;
        this.inspectStatus = GoodsLog.findByGoods(goods) == null?null:GoodsLog.findByGoods(goods).status.toString();

    }

    public static GoodsVO fromApp(Goods goods){
        GoodsLog goodsLog =  GoodsLog.findByGoods(goods);
        if(goodsLog != null){
            GoodsVO goodsVO = new GoodsVO();
            goodsVO.id = goods.id;
            goodsVO.areaId = goods.area.id;
            goodsVO.number = goods.number;
            goodsVO.name = goods.name;
            goodsVO.type = goods.type;
            goodsVO.snumber = goods.snumber;
            goodsVO.status = goods.status.toString();
            goodsVO.createdate= goods.createdate;
            goodsVO.usedate = goods.usedate;
            goodsVO.repairdate = goods.repairdate;
            goodsVO.scrapdate = goods.scrapdate;
            goodsVO.imgUrl = goods.imgUrl;
            goodsVO.cycleTime = goods.cycleTime;
            goodsVO.validTime = goods.validTime;
            goodsVO.inspectStatus =goodsLog.status.toString();
            return goodsVO;
        }else{
            return  null;
        }

    }

    public static List<GoodsVO> convertFromGoods(List<Goods> list){
        List<GoodsVO> goodsVOS = new ArrayList<>();
        list.stream().forEach(g -> {
            GoodsVO vo = new GoodsVO(g);
            goodsVOS.add(vo);
        });
        return  goodsVOS;
    }


    public static List<GoodsVO> convertFromGoodsLog(List<Goods> list){
        List<GoodsVO> goodsVOS = new ArrayList<>();
        list.stream().forEach(g -> {
            GoodsVO vo = fromApp(g);
            if(vo != null){
                goodsVOS.add(vo);
            }
        });
        return  goodsVOS;
    }


    public static List<GoodsVO> convertFromLog(List<GoodsLog> list){
        List<GoodsVO> goodsVOS = new ArrayList<>();
        list.stream().forEach(g -> {
            GoodsVO vo = new GoodsVO();
            vo.areaId = g.goods.area.id;
            vo.areaName = g.goods.area.name;
            vo.areaAddress = g.goods.area.address;
            vo.name = g.goods.name;
            vo.number = g.goods.number;
            if(g.rstatus != null){
                vo.shStatus = g.rstatus.toString();
            }
            goodsVOS.add(vo);
        });
        return  goodsVOS;
    }
}
