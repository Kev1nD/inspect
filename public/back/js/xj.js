/**
 * Created by daikai on 2017/7/8.
 */



$(function(){

    $(".editLmain-left").css("height",function(){
        return $(this).next(".editLmain-right").outerHeight();
    })


    function isPhoneNo(phone) {
        var pattern = /^1[34578]\d{9}$/;
        return pattern.test(phone);
    }





    layer.photos({
        photos: '.js-td-photo',
        shade: [0.1,'#fff']
        ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
    });

    //退出平台
    $("#main-navbar").on("click",".logout",function(){
        $.get("/manage/logout",function(json){
            window.location.href = json.data;
        })
    })

    //修改密码
    $("#main-navbar").on("click",".edit-passwd",function(){
        layer.open({
            type: 1,
            area: ['400px', '270px'],
            title: '修改密码',
            content: "<div class='edit-passwd-box'>" +
                        "<ul>"+
                            "<li><span>原密码：</span><input type='password' id='oldPwd'></li>" +
                            "<li><span>新密码：</span><input type='password' id='newPwd'></li>" +
                            "<li><span>确认密码：</span><input type='password' id='confirmPwd'></li>" +
                        "</ul>"+
                    "</div>",
            btn: ['保存','返回'],
            yes:function(index){
                var oldPwd = $("#oldPwd").val().trim(),
                    newPwd = $("#newPwd").val().trim(),
                    confirmPwd = $("#confirmPwd").val().trim();
                if(oldPwd.length < 1){
                    layer.msg("原密码不能为空！");
                    return;
                }
                if(newPwd.length < 1){
                    layer.msg("新密码不能为空！");
                    return;
                }
                if(confirmPwd.length < 1){
                    layer.msg("确认密码不能为空！");
                    return;
                }
                if(newPwd != confirmPwd){
                    layer.msg("确认密码与新密码不一致！");
                    return;
                }
                $.post("/manage/editPasswd",{oldPwd:oldPwd,newPwd:newPwd},function(json){
                     if(json.status == "true"){
                         layer.msg('修改密码成功！', {icon: 1});
                         layer.close(index);
                     }else{
                         layer.msg('原密码不正确！', {icon: 2});
                     }

                })




            }
        })
    })


    $("#cycleTime").on("change",function(){
        if($("#cycleTime option:selected").attr("id") == "0"){
            $("#cycleMonthDIV").css("display","block");
        }else {
            $("#cycleMonthDIV").css("display","none");
        }
    })



    $(".js-cancel-add-org").on("click",function(){
        window.location.href = "/manage/index?groupId=1";
    });

    if($("#province").length > 0){
        $.post("/manage/getProvinces",function(json){
            var optionStr = "";
            for(var key in json){
                var id = json[key].id,name=json[key].name;
                optionStr +="<option id='"+id+"'>"+name+"</option>"
            }
            $("#province").append(optionStr);

        })

        $("#province").off("change").on("change",function(){
            $("#city").html("");
            $("#district").html("");
            $("#street").html("");
            var parentId = $("#province option:selected").attr("id");
            $.get("/manage/getCityAndDistrict",{parentId:parentId},function(json){
                var optionStr = "<option>-请选择-</option>";
                for(var key in json){
                    var id = json[key].id,name=json[key].name;
                    optionStr +="<option id='"+id+"'>"+name+"</option>"
                }
                $("#city").html("");
                $("#city").append(optionStr);

            })
        })


        if($("#city").attr("data-id")){
            var parentId = $("#city").attr("data-id");
            $.get("/manage/getCityAndDistrict",{parentId:parentId},function(json){
                var optionStr = "";
                for(var key in json){
                    var id = json[key].id,name=json[key].name;
                    optionStr +="<option id='"+id+"'>"+name+"</option>"
                }
                //$("#city").html("");
                $("#city").append(optionStr);

            })
        }




        $("#city").off("change").on("change",function(){
            var parentId = $("#city option:selected").attr("id");
            $.get("/manage/getCityAndDistrict",{parentId:parentId},function(json){
                var optionStr = "<option>-请选择-</option>";
                for(var key in json){
                    var id = json[key].id,name=json[key].name;
                    optionStr +="<option id='"+id+"'>"+name+"</option>"
                }
                $("#district").html("");
                $("#district").append(optionStr);

            })
        })



        if($("#district").attr("data-id")){
            var parentId = $("#district").attr("data-id");
            $.get("/manage/getStreet",{parentId:parentId},function(json){
                var optionStr = "";
                for(var key in json){
                    var id = json[key].id,name=json[key].name;
                    optionStr +="<option id='"+id+"'>"+name+"</option>"
                }
                //$("#district").html("");
                $("#district").append(optionStr);

            })
        }

        // if($("#street").attr("data-id")){
        //     var parentId = $("#street").attr("data-id");
        //     $.get("/manage/getStreet",{parentId:parentId},function(json){
        //         var optionStr = "";
        //         for(var key in json){
        //             var id = json[key].id,name=json[key].name;
        //             optionStr +="<option id='"+id+"'>"+name+"</option>"
        //         }
        //         //$("#street").html("");
        //         $("#street").append(optionStr);
        //
        //     })
        // }

        $("#district").off("change").on("change",function(){
            var parentId = $("#district option:selected").attr("id"),
                district = $("#district option:selected").val();
            $.get("/manage/getStreet",{parentId:parentId},function(json){
                var optionStr = "<option>-请选择-</option>";
                for(var key in json){
                    var id = json[key].id,name=json[key].name;
                    optionStr +="<option id='"+id+"'>"+name+"</option>"
                }
                $("#street").html("");
                $("#street").append(optionStr);

            })
        })
    }


    //新增企业
    $(".js-add-org").on("click",function(){

        var orgName = $("#orgName").val().trim();
        if (orgName.length < 1){
            layer.msg("企业名称未填写！");
            return;
        }

        var orgAdminName = $("#orgAdminName").val().trim();
        if (orgAdminName.length < 1){
            layer.msg("企业管理员账号未填写！");
            return;
        }

        var orgAddress = $("#orgAddress").val().trim();
        if (orgAddress.length < 1){
            layer.msg("企业详细地址未填写！");
            return;
        }

        var types = [];
        $(".js-org-type li").each(function(){
            if($(this).find("input").prop("checked")){
                types.push($(this).find("span").text());
            }
        })
        if (types.length < 1){
            layer.msg("企业类型未勾选！");
            return;
        }
        if (types.length > 3){
            layer.msg("企业类型最多勾选三个！");
            return;
        }

        var regionId = $("#regionId").val();


        var licence = $("#yyimg").attr("src");
        if(licence.length <1){
            layer.msg("企业营业执照未上传！");
            return;
        }

        var floorpics = [];
        $(".js-floor-pics").each(function(){
            floorpics.push($(this).attr("data-id"));
        })

        $.ajax({
            url:"/manage/addOrg",
            data:{
                "orgVo.name":orgName,
                "orgVo.type":types.join(","),
                "orgVo.regionId":regionId,
                "orgVo.orgAdminName":orgAdminName,
                "orgVo.orgAddress":orgAddress,
                "orgVo.buildTime":$("#buildTime").val().trim(),
                "orgVo.structure":$("#structure").val().trim(),
                "orgVo.estate":$("#estate").val().trim(),
                "orgVo.area":$("#range").val().trim(),
                "orgVo.product":$("#product").val().trim(),
                "orgVo.personNum":$("#personNum").val().trim(),
                "orgVo.scale":$("#scale").val().trim(),
                "orgVo.site":$("#site").val().trim(),
                "orgVo.intro":$("#intro").val().trim(),
                "orgVo.survey":$("#survey").val().trim(),
                "orgVo.licence":licence,
                "orgVo.permit":$("#xkimg").attr("src"),
                "orgVo.districtUrl":$("#dlimg").attr("src"),
                "orgVo.picIds":floorpics.length < 1?"":floorpics.join(",")

            },
            type:"post",
            success:function(json){
                if (json.status == "true"){
                    layer.msg("新增企业成功！");
                    setTimeout(function(){
                        window.location.href = "/manage/index?groupId=1";
                    },100);
                }else{
                    layer.msg(json.data);
                }
            }

        })

    })



    //编辑企业
    $(".js-edit-org").on("click",function(){
        var orgId = $(this).attr("data-id");

        var orgName = $("#orgName").val().trim();
        if (orgName.length < 1){
            layer.msg("企业名称未填写！");
            return;
        }

        var orgAddress = $("#orgAddress").val().trim();
        if (orgAddress.length < 1){
            layer.msg("企业详细地址未填写！");
            return;
        }


        var types = [];
        $(".js-org-type li").each(function(){
            if($(this).find("input").prop("checked")){
                types.push($(this).find("span").text());
            }
        })
        if (types.length < 1){
            layer.msg("企业类型未勾选！");
            return;
        }
        if (types.length > 3){
            layer.msg("企业类型最多勾选三个！");
            return;
        }

        var regionId = $("#street option:selected").attr("id");
        if(!regionId){
            layer.msg("企业地址请精确到街道！");
            return;
        }

        var licence = $("#yyimg").attr("src");
        if(licence.length <1){
            layer.msg("企业营业执照未上传！");
            return;
        }

        var floorpics = [];
        $(".js-floor-pics").each(function(){
            floorpics.push($(this).attr("data-id"));
        })

        $.ajax({
            url:"/manage/editOrg",
            data:{
                "orgVo.id":orgId,
                "orgVo.name":orgName,
                "orgVo.type":types.join(","),
                "orgVo.regionId":regionId,
                "orgVo.orgAddress":orgAddress,
                "orgVo.buildTime":$("#buildTime").val().trim(),
                "orgVo.structure":$("#structure").val().trim(),
                "orgVo.estate":$("#estate").val().trim(),
                "orgVo.area":$("#range").val().trim(),
                "orgVo.product":$("#product").val().trim(),
                "orgVo.personNum":$("#personNum").val().trim(),
                "orgVo.scale":$("#scale").val().trim(),
                "orgVo.site":$("#site").val().trim(),
                "orgVo.intro":$("#intro").val().trim(),
                "orgVo.survey":$("#survey").val().trim(),
                "orgVo.licence":licence,
                "orgVo.permit":$("#xkimg").attr("src"),
                "orgVo.districtUrl":$("#dlimg").attr("src"),
                "orgVo.picIds":floorpics.length < 1?"":floorpics.join(",")

            },
            type:"post",
            success:function(json){
                if (json.status == "true"){
                    layer.msg("编辑企业成功！");
                    setTimeout(function(){
                        window.location.href = "/manage/index";
                    },100);
                }else{
                    layer.msg(json.data);
                }
            }

        })

    })


    $("#addUser").on("click",function(){

        layer.open({
            type: 1,
            area: ['500px', '500px'],
            title: '新增人员',
            content: $('.layer-userAdd'),
            btn: ['保存','返回'],
            yes: function(index, layero){
                var mobile =$("#mobile").val().trim();
                if(!isPhoneNo(mobile)){
                    layer.msg("手机格式不正确！");
                    return;
                }

                var job = $("#job").val().trim();
                if(job == "巡检员"){
                    if(!$("#fzer option:selected").attr("id")){
                        layer.msg("负责人不能为空！");
                        return;
                    }
                }

                var username = $("#username").val().trim();
                if(username.length < 1){
                    layer.msg("账号不能为空！");
                    return;
                }


                $.ajax({
                    url:"/manage/addPerson",
                    data:{
                        roleId:$("#orgRole option:selected").attr("id"),
                        orgId:$('.layer-userAdd').attr("data-id"),
                        "personVO.username":username,
                        "personVO.name":$("#personName").val().trim(),
                        "personVO.sex":$("input[name='sex']:checked").val(),
                        "personVO.job":job,
                        "personVO.password":$("#password").val().trim(),
                        "personVO.mobile":mobile,
                        "personVO.contact":$("#contact").val().trim(),
                        "personVO.officedate":$("#officedate").val().trim(),
                        "personVO.company":$("#company").val().trim(),
                        "personVO.description":$("#description").val().trim(),
                        "personVO.remark":$("#remark").val().trim(),
                        "personVO.fuzePersonId":$("#fzer option:selected").attr("id"),
                        "personVO.avator":$("#tximg").attr("src")
                    },
                    type:"post",
                    success:function(json){
                        if (json.status == "true"){
                            layer.msg("新增人员成功！");
                            window.location.reload();
                        }else{
                            layer.msg("手机号码已经被注册！");

                        }
                    }

                })
            },
            no: function(index, layero){
                layer.close(index);
            }
        });
    })



    $("#addOrgUser").on("click",function() {
        var orgId = $("#groups option:selected").attr("value");

        if (!orgId || orgId == "0") {
            layer.msg("请先选择机构！", {icon: 2});
            return;
        }


    $.get("/manage/addOrgPersonPage",{orgId:orgId},function(json){
        if(json.succ == "true"){
        layer.open({
            type: 1,
            area: ['500px', '500px'],
            title: '新增人员',
            content: json.html,
            btn: ['保存', '返回'],
            yes: function (index, layero) {
                var mobile = $("#org-mobile").val().trim();
                if (!isPhoneNo(mobile)) {
                    layer.msg("手机格式不正确！");
                    return;
                }

                var job = $("#org-job").val().trim();
                if (job == "巡检员") {
                    if (!$("#org-fzer option:selected").attr("id")) {
                        layer.msg("负责人不能为空！");
                        return;
                    }
                }


                var username = $("#org-username").val().trim();
                if(username.length < 1){
                    layer.msg("登录账号不能为空！");
                    return;
                }
                $.ajax({
                    url: "/manage/addPerson",
                    data: {
                        roleId: $("#org-orgRole option:selected").attr("id"),
                        orgId: $('.org-layer-userAdd').attr("data-id"),
                        "personVO.username": $("#org-username").val().trim(),
                        "personVO.name": $("#org-personName").val().trim(),
                        "personVO.sex": $("input[name='org-sex']:checked").val(),
                        "personVO.job": job,
                        "personVO.password": $("#org-password").val().trim(),
                        "personVO.mobile": mobile,
                        "personVO.contact": $("#org-contact").val().trim(),
                        "personVO.officedate": $("#org-officedate").val().trim(),
                        "personVO.description": $("#org-description").val().trim(),
                        "personVO.remark": $("#org-remark").val().trim(),
                        "personVO.fuzePersonId": $("#org-fzer option:selected").attr("id"),
                        "personVO.avator": $("#org-tximg").attr("src")
                    },
                    type: "post",
                    success: function (json) {
                        if (json.status == "true") {
                            layer.msg("新增人员成功！");
                            window.location.reload();
                        } else {
                            layer.msg("手机号码已经被注册！");

                        }
                    }

                })
            },
            no: function (index, layero) {
                layer.close(index);
            }
        });

            //职位是巡检原
            $("#org-job").on("change",function(){
                var job = $("#org-job option:selected").val();
                if(job == "巡检员"){
                    $("#org-fzerItem").css("display","block");
                }else
                    $("#org-fzerItem").css("display","none");
            })


     }
    })
})



    $("#addSupplier").on("click",function(){

        layer.open({
            type: 1,
            area: ['500px', '500px'],
            title: '新增供应商',
            content: $('.layer-userAdd'),
            btn: ['保存','返回'],
            yes: function(index, layero){
                var suppName = $("#cname").val().trim(),
                    registNum =$("#registNum").val().trim();
                if(suppName.length <1){
                    layer.msg("供应商名称不能为空！");
                    return;
                }
                if(registNum.length <1){
                    layer.msg("供应商注册号不能为空！");
                    return;
                }
                $.ajax({
                    url:"/manage/addSupplier",
                    data:{
                        orgId:$('.layer-userAdd').attr("data-id"),
                        "supplierVO.type":$("#ctype").val().trim(),
                        "supplierVO.name":suppName,
                        "supplierVO.address":$("#caddress").val().trim(),
                        "supplierVO.registNum":registNum,
                        "supplierVO.job":$("#job").val().trim(),
                        "supplierVO.isQualified":$("#isQualified option:selected").val(),
                        "supplierVO.mobile":$("#mobile").val().trim(),
                        "supplierVO.contact":$("#contact").val().trim(),
                        "supplierVO.manager":$("#manager").val().trim(),
                        "supplierVO.manageMobile":$("#manageMobile").val().trim(),
                        "supplierVO.licence":$("#yyimg").attr("src"),
                        "supplierVO.permit":$("#xkimg").attr("src"),
                        "supplierVO.remark":$("#remark").val().trim()
                    },
                    type:"post",
                    success:function(json){
                        if (json.status == "true"){
                            layer.msg("新增供应商成功！");
                            window.location.reload();
                        }
                    }

                })
            },
            no: function(index, layero){
                layer.close(index);
            }
        });
    })





    $("#addArea").on("click",function(){
        layer.open({
            type: 1,
            area: ['500px', '500px'],
            title: '新增检查点',
            content: $('.layer-userAdd'),
            btn: ['保存','返回'],
            yes: function(index, layero){
                $.ajax({
                    url:"/manage/addArea",
                    data:{
                        orgId:$('.layer-userAdd').attr("data-id"),
                        "areaVO.number":$("#number").val().trim(),
                        "areaVO.name":$("#areaName").val().trim(),
                        "areaVO.address":$("#address").val().trim(),
                        "areaVO.checkerId":$("#checker option:selected").attr("id"),
                        "areaVO.remark":$("#remark").val().trim(),
                        "areaVO.imageUrl":$("#areaimg").attr("src")
                    },
                    type:"post",
                    success:function(json){
                        if (json.status == "true"){
                            layer.msg("新增检查点成功！");
                            window.location.reload();
                        }else{
                            layer.msg("检查点编号不能重复！");
                        }
                    }

                })
            },
            no: function(index, layero){
                layer.close(index);
            }
        });
    })

    $("#addOrgArea").on("click",function(){
        var orgId = $("#groups option:selected").attr("value");

        if(!orgId || orgId == "0"){
            layer.msg("请先选择机构！", {icon: 2});
            return;
        }

        $.get("/manage/addPlatAreaPage",{orgId:orgId},function(json){
            if(json.succ == "true"){

                layer.open({
                    type: 1,
                    area: ['500px', '500px'],
                    title: '新增检查点',
                    content: json.html,
                    btn: ['保存','返回'],
                    yes: function(index, layero){
                        $.ajax({
                            url:"/manage/addArea",
                            data:{
                                orgId:orgId,
                                "areaVO.number":$("#pl-number").val().trim(),
                                "areaVO.name":$("#pl-areaName").val().trim(),
                                "areaVO.address":$("#pl-address").val().trim(),
                                "areaVO.checkerId":$("#pl-checker option:selected").attr("id"),
                                "areaVO.remark":$("#pl-remark").val().trim(),
                                "areaVO.imageUrl":$("#pl-areaimg").attr("src")
                            },
                            type:"post",
                            success:function(json){
                                if (json.status == "true"){
                                    layer.msg("新增检查点成功！");
                                    window.location.reload();
                                }
                            }

                        })
                    },
                    no: function(index, layero){
                        layer.close(index);
                    }
                });


                $("#pl-areafile").on("change",function(){
                    var formData = new FormData($("#pl-areaForm")[0]);
                    $.ajax({
                        url:"/manage/uploadFile",
                        data:formData,
                        type:"post",
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(json){
                            $("#pl-areaimg").css("display","block");
                            $("#pl-areaimg").attr("src",json.data);
                        }


                    })
                })

            }
        })

    })


    $("#addGoods").on("click",function(){
        layer.open({
            type: 1,
            area: ['500px', '500px'],
            title: '新增检查对象',
            content: $('.layer-userAdd'),
            btn: ['保存','返回'],
            yes: function(index, layero){
                var repairPeriod = $("#repairPeriod").val().trim(),
                    validTime = $("#validTime").val().trim(),
                    goodsName = $("#goodsName option:selected").val().trim(),
                    goodsType = $("#goodsType").val().trim(),
                    scrapdate = $("#scrapdate").val().trim(),
                    areaId = $("#area option:selected").attr("id");

                var abnormalIds  = [];
                $("#abNromal li").each(function(){
                    if($(this).find("input").prop("checked")){
                        abnormalIds.push($(this).find("input").attr("data-id"));
                    }
                })
                console.log(abnormalIds)

                if(goodsName.length < 1){
                    layer.msg("对象名称未填写！");
                    return;
                }
                if(goodsType.length < 1){
                    layer.msg("对象型号未填写！");
                    return;
                }
                if(abnormalIds.length < 1){
                    layer.msg("非正常选项未勾选！");
                    return;
                }
                if(scrapdate.length < 1){
                    layer.msg("报废日期未填写！");
                    return;
                }
                if(repairPeriod.length < 1){
                    layer.msg("检修日期未填写！");
                    return;
                }




                if(validTime =="" ||  isNaN(validTime)){
                    layer.msg("巡检有效期必须是数字！");
                    return;
                }
                if(!areaId){
                    layer.msg("请先选择巡检地点！");
                    return;
                }

                var cycleTime = $("#cycleTime option:selected").attr("id");
                if(cycleTime == "0"){
                    cycleTime  = $("#cycleMonth").val().trim();
                    if(cycleTime.length < 1){
                        layer.msg("具体月数不能为空！");
                        return;
                    }

                    //正则判断是否是正整数
                    var reg = new RegExp("^[0-9]*$");
                    if(parseInt(cycleTime) < 1 || !reg.test(cycleTime)){
                        layer.msg("具体月数必须是大于0的整数！");
                        return;
                    }


                }


                $.ajax({
                    url:"/manage/addGoods",
                    data:{
                        orgId:$('.layer-userAdd').attr("data-id"),
                        "goodsVO.number":$("#number").val().trim(),
                        "goodsVO.name":goodsName,
                        "goodsVO.type":goodsType,
                        "goodsVO.abnormalIds":abnormalIds.join(","),
                        "goodsVO.snumber":$("#snumber").val().trim(),
                        "goodsVO.createdate":$("#createdate").val().trim(),
                        "goodsVO.usedate":$("#usedate").val().trim(),
                        "goodsVO.scrapdate":scrapdate,
                        "goodsVO.repairPeriod":repairPeriod,
                        "goodsVO.cycleTime":cycleTime,
                        "goodsVO.validTime":validTime,
                        "goodsVO.producerId":$("#supplier option:selected").attr("id"),
                        "goodsVO.supplyerId":$("#gyorg option:selected").attr("id"),
                        "goodsVO.areaId":areaId,
                        "goodsVO.imgUrl":$("#goodsimg").attr("src")
                    },
                    type:"post",
                    success:function(json){
                        if (json.status == "true"){
                            layer.msg("新增检查对象成功！");
                            window.location.reload();
                        }
                    }

                })
            },
            no: function(index, layero){
                layer.close(index);
            }
        });
    })


    $("#addOrgGoods").on("click",function(){
        var orgId = $("#groups option:selected").attr("value");

        if(!orgId || orgId == "0"){
            layer.msg("请先选择机构！", {icon: 2});
            return;
        }

        $.get("/manage/addPlatGoodsPage",{orgId:orgId},function(json){
            if(json.succ == "true"){
                layer.open({
                    type: 1,
                    area: ['500px', '500px'],
                    title: '新增检查对象',
                    content: json.html,
                    btn: ['保存','返回'],
                    yes: function(index, layero){
                        var repairPeriod = $("#pl-repairPeriod").val().trim(),
                            validTime = $("#pl-validTime").val().trim(),
                            goodsName = $("#pl-goodsName").val().trim(),
                            goodsType = $("#pl-goodsType").val().trim(),
                            scrapdate = $("#pl-scrapdate").val().trim(),
                            areaId = $("#pl-area option:selected").attr("id");

                        if(goodsName.length < 1){
                            layer.msg("对象名称未填写！");
                            return;
                        }
                        if(goodsType.length < 1){
                            layer.msg("对象型号未填写！");
                            return;
                        }
                        if(scrapdate.length < 1){
                            layer.msg("报废日期未填写！");
                            return;
                        }

                        if(repairPeriod.length < 1){
                            layer.msg("检修日期未填写！");
                            return;
                        }



                        if(validTime =="" ||  isNaN(validTime)){
                            layer.msg("巡检有效期必须是数字！");
                            return;
                        }
                        if(!areaId){
                            layer.msg("请先选择巡检地点！");
                            return;
                        }

                        var abnormalIds  = [];
                        $("#pl-abNromal li").each(function(){
                            if($(this).find("input").prop("checked")){
                                abnormalIds.push($(this).find("input").attr("data-id"));
                            }
                        })


                        var cycleTime = $("#pl-cycleTime option:selected").attr("id");
                        if(cycleTime == "0"){
                            cycleTime  = $("#pl-cycleMonth").val().trim();
                            if(cycleTime.length < 1){
                                layer.msg("具体月数不能为空！");
                                return;
                            }

                            //正则判断是否是正整数
                            var reg = new RegExp("^[0-9]*$");
                            if(parseInt(cycleTime) < 1 || !reg.test(cycleTime)){
                                layer.msg("具体月数必须是大于0的整数！");
                                return;
                            }


                        }


                        $.ajax({
                            url:"/manage/addGoods",
                            data:{
                                orgId:orgId,
                                "goodsVO.number":$("#pl-number").val().trim(),
                                "goodsVO.name":goodsName,
                                "goodsVO.type":goodsType,
                                "goodsVO.abnormalIds":abnormalIds.join(","),
                                "goodsVO.snumber":$("#pl-snumber").val().trim(),
                                "goodsVO.createdate":$("#pl-createdate").val().trim(),
                                "goodsVO.usedate":$("#pl-usedate").val().trim(),
                                "goodsVO.scrapdate":scrapdate,
                                "goodsVO.repairPeriod":repairPeriod,
                                "goodsVO.cycleTime":cycleTime,
                                "goodsVO.validTime":validTime,
                                "goodsVO.producerId":$("#pl-supplier option:selected").attr("id"),
                                "goodsVO.areaId":areaId,
                                "goodsVO.imgUrl":$("#pl-goodsimg").attr("src")
                            },
                            type:"post",
                            success:function(json){
                                if (json.status == "true"){
                                    layer.msg("新增检查对象成功！");
                                    window.location.reload();
                                }
                            }

                        })

                    },
                    no: function(index, layero){
                        layer.close(index);
                    }


                });

                $("#pl-goodsfile").on("change",function(){
                    var formData = new FormData($("#pl-goodsForm")[0]);
                    $.ajax({
                        url:"/manage/uploadFile",
                        data:formData,
                        type:"post",
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(json){
                            $("#pl-goodsimg").css("display","block");
                            $("#pl-goodsimg").attr("src",json.data);
                        }


                    })
                })

                $("#pl-cycleTime").on("change",function(){
                    if($("#pl-cycleTime option:selected").attr("id") == "0"){
                        $("#pl-cycleMonthDIV").css("display","block");
                    }else {
                        $("#pl-cycleMonthDIV").css("display","none");
                    }
                })


            }

        })



    })


    //删除巡检对象
    $(".js-del-goods").on("click",function(){
        var curNode = $(this).parents("tr"),
            goodsId = curNode.attr("data-id");

        layer.confirm("您确定要删除该巡检对象记录吗？",function(){
            $.post("/manage/delGoods",{goodsId:goodsId},function(json){
                if(json.status == "true"){
                    curNode.remove();
                    layer.msg("删除成功！")
                }
            })
        })
    });


    //维修
    $(".js-repair-goods").on("click",function(){
        var curNode = $(this).parents("tr"),
            goodsId = curNode.attr("data-id");

        $.get("/manage/repairPage",{goodsId:goodsId},function(json){
            if(json.succ == "true"){
                layer.open({
                    type: 1,
                    area: ['500px', '300px'],
                    title: '维修',
                    btn: ['确定', '取消'],
                    content: json.html,
                    yes:function(index){
                        var content = $("#content").val().trim();
                        if(content.length < 1){
                            layer.msg("请输入维修内容!");
                            return;
                        }
                        var supplierId = $("#chooseRepairOrg option:selected").attr("id");
                        if(!supplierId){
                            layer.msg("请选择维修公司!");
                            return;
                        }
                        $.post("/manage/repairGoods",{goodsId:goodsId,supplierId:supplierId,content:content},function(json){
                            if(json.status == "true"){
                                layer.msg("维修提交成功！");
                                layer.close(index);
                                window.location.reload();
                            }
                        })

                    },
                    no:function(index){
                        layer.close(index);
                    }
                })
            }
        })

    })




    //维修记录查看
    $(".js-repair-log").on("click",function(){
        var curNode = $(this).parents("tr"),
            goodsId = curNode.attr("data-id");

        $.get("/manage/repairLogPage",{goodsId:goodsId},function(json){
            if(json.succ == "true"){
                layer.open({
                    type: 1,
                    area: ['500px', '500px'],
                    title: '维修记录',
                    content: json.html
                })


                $(".js-del-repair-log").on("click",function(){
                    var logId = $(this).attr("data-id"),
                        curNode = $(this).parents("li");
                    layer.confirm("您确定要删除该条维修记录吗？",function(index){
                        $.post("/manage/delRepairLog",{logId:logId},function(json){
                            if(json.status == "true"){
                                layer.close(index);
                                curNode.remove();
                                layer.msg("删除成功！");
                            }
                        })
                    })

                })



                return false;
            }
        })
        return false;

    })



    $("#statis1Table").on("click",".js-view-fzper",function(){
        var orgId = $(this).attr("data-id");
        $.get("/tj/fzPersPage",{orgId:orgId},function(json){
            if(json.succ == "true"){
                layer.open({
                    type: 1,
                    area: ['500px', '500px'],
                    title: '查看负责人',
                    content: json.html,
                    btn: ['确认'],
                    yes: function(index){
                        layer.close(index);
                    },
                    no:function(index){
                        layer.close(index);
                    }
                })
            }
        })
    })



})